<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<c:set var="path" value="${pageContext.servletContext.contextPath}"></c:set>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Panel</title>

    <link href="${path}/resources/static/css/boot/bootstrap.min.css" rel="stylesheet">
    <link href="${path}/resources/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${path}/resources/static/css/animate.css" rel="stylesheet">
    <link href="${path}/resources/static/css/style.css" rel="stylesheet">
    <link href="${path}/resources/static/css/newStyle.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
</head>

<body>
<div id="wrapper">

    <nav class="navbar navbar-fixed-top white-bg header-bg clearfix" role="navigation" style="margin-bottom: 0">
        <div class="company-name clearfix">
    <!--         <h2> RAS/BRAS  </h2> -->
    <img alt="" src="${path}/resources/static/images/logo.png">
        </div>
    <!--     <div class="navbar-header kc">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div> -->
        <div class="logo-dashboard">
 <!--            <h2>RAS RAS</h2> -->

        </div>
    </nav>