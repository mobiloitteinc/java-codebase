
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<c:set var="path" value="${pageContext.servletContext.contextPath}"></c:set>
	<jsp:include page="header.jsp"></jsp:include>

       <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
    	<ul class="nav metismenu" id="side-menu">

			<li><a href="userManagement">Users</a></li>
			<li class="active special_link"><a href="staticContent" >Static Content</a></li>

		<li><a  class="nav-menu"  id="logout"><i class="fa fa-sign-out nav-label"></i> <span class="nav-label">Logout</span></a></li>
					
		</ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h2>Static Content Management</h2>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-common table-striped table-bordered table-hover mb0">
                                    <thead>
                                    <tr>
                                        <th style="width:70px;">S. No.</th>
                                        <th>Page name</th>

                                        <th style="width:100px; text-align:center;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>About Us</td>

                                        <td class="text-center">
                                            <!-- <a href="about-us.html" class="btn btn-xs btn-primary">View</a> -->
                                            <a onclick="getParticularStaticContent('aboutUs','About Us')">
                                                View
                                            </a>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Terms & Services</td>

                                        <td class="text-center">
                                            <!--  <a href="terms-services.html" class="btn btn-xs btn-primary">View</a> -->
                                            <a onclick="getParticularStaticContent('termsAndServices','Terms and Services')">
                                                View
                                            </a>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Privacy Policy</td>

                                        <td class="text-center">
                                            <!-- <a href="terms-services.html" class="btn btn-xs btn-primary">View</a> -->
                                            <a onclick="getParticularStaticContent('privacyPolicy','Privacy Policy')">
                                                View
                                            </a>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Help</td>

                                        <td class="text-center">
                                            <!-- <a href="terms-services.html" class="btn btn-xs btn-primary">View</a> -->
                                            <a onclick="getParticularStaticContent('help','Help')">
                                                View
                                            </a>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<div id="editusermodal" class="modal fade global-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Static Content</h4>
            </div>
            <div class="modal-body">
                <form class="modal-form change-pwd-form">
                    <div class="form-group clearfix">
                        <label for="current_password" class="col-sm-4 control-label">Page Name</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="Category name" id="staticContentTitle"
                                   readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label for="current_password" class="col-sm-4 control-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="staticContentDescription"
                                      style="width: 349px; height: 182px; margin: 0px -0.34375px 0px 0px;resize: none;"></textarea>
                            <span id="staticContentDescriptionerror" class="error" Style="color: red;"></span>
                        </div>
                    </div>
                    <input class="form-control" type="hidden" placeholder="Category name" id="staticStatus"
                           readonly="readonly"/>
                    <input class="form-control" type="hidden" placeholder="Category name" id="languageType"
                           readonly="readonly"/>
                    <input class="form-control" type="hidden" placeholder="Category name" id="id" readonly="readonly"/>

                    <div class="form-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" id="updateStaticContent" class="btn btn-success">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
	<jsp:include page="footer.jsp"></jsp:include>
	<script src="${path}/resources/static/js/admin/static-content.js"></script>
