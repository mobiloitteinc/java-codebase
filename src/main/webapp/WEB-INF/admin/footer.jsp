<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<c:set var="path" value="${pageContext.servletContext.contextPath}"></c:set>
      <div class="footer text-center">
            <p>RasRas 2017. All Right Reserved.</p>
        </div>
    </div>
</div>


<div class="container">
  <!-- <h2>Logout</h2> -->
  <!-- Trigger the modal with a button -->


  <!-- Modal -->
  <div class="modal fade" id="logout-modal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Logout</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to logout?</p>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-danger" id="yes-logout">Yes</button>
          <button type="button" class="btn btn-green" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- MOdal End -->

<div class="container">
  <!-- <h2>Logout</h2> -->
  <!-- Trigger the modal with a button -->


  <!-- Modal -->
  <div class="modal fade" id="success-modal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <p>Your information has been updated successfully.</p>
        </div>
        <div class="modal-footer">
             <button type="button" class="btn btn-green" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="success-user-update-modal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <p>Your information has been updated successfully.</p>
        </div>
        <div class="modal-footer">
             <button type="button" class="btn btn-green" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- Mainly scripts -->
<script src="${path}/resources/static/js/jquery-3.1.1.min.js"></script>
<script src="${path}/resources/static/js/bootstrap.min.js"></script>
<script src="${path}/resources/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="${path}/resources/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="${path}/resources/static/js/inspinia.js"></script>
<script src="${path}/resources/static/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="${path}/resources/static/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    
    
 $(document).ready(function(){
	// $("#userTable").DataTable({"bStateSave": true});
			 $("#userTable").DataTable();
	 $("#logout").click(function(){
	
	    	$("#logout-modal").modal('show')
	    })
	    
	    $("#yes-logout").click(function(){
	    	window.location.href="logout";
	    })
	    
 })
 
    
</script>
</body>
</html>
