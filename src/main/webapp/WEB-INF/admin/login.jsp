<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<c:set var="path" value="${pageContext.servletContext.contextPath}"></c:set>
	<jsp:include page="header.jsp"></jsp:include>

<div class="middle-box ibox-content text-center loginscreen animated fadeInDown  padd0">

    <div class="login-content">
   
        <form class="m-t" role="form" action="authenticateLogin" method="post">
            <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address"
                       required="">
                <span id="emailerr" style="color: red"></span>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" id="password" placeholder="Password"
                       required="">
                <span id="passworderr" style="color: red"></span>
                <span style="color: red"><c:if test="${wrongcreditnial!=null}">
			<p>
                    <c:out value="${wrongcreditnial}"/>
                    <p>
                </c:if></span>
            </div>

            <button type="submit" id="submit" class="btn btn-primary block full-width m-b">Submit</button>


        </form>
    </div>
</div>



	<jsp:include page="footer.jsp"></jsp:include>
<script>
$(document).ready(function(){

	$(".navbar-header .kc div").remove()
	

})

</script>

