
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="path" value="${pageContext.servletContext.contextPath}"></c:set>
<jsp:include page="header.jsp"></jsp:include>
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">

		<ul class="nav metismenu" id="side-menu">

			<li class="active special_link"><a href="userManagement" >Users</a></li>
			<li><a href="staticContent">Static Content</a></li>

			<li><a  class="nav-menu"  id="logout"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a></li>
		</ul>
	</div>
</nav>

<div id="page-wrapper" class="gray-bg">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h2>User Management</h2>
					</div>

					<div class="ibox-content">

						<div class="table-responsive">
							<table
								class="table table-common table-striped table-bordered table-hover mt5 mb0"
								id="userTable">
								<thead>
									<tr>
										<th>S. No.</th>
										<th>Name</th>
										<th>Email</th>
										<th>Phone Number</th>
										<th>Status</th>
										<th>Creation Date</th>
										<!--   <th style="width:100px; text-align:center;" >Status</th> -->
										<th style="width: 100px; text-align: center;">Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${data}" begin="0" step="1" varStatus="loop">
										<tr>
											<td>${loop.count}</td>
											<td>${list.name}</td>
											<td>${list.email}</td>
											<td>${list.mobileNumber}</td>
											<c:choose>
												<c:when test="${list.status}">
													<td>Active</td>
												</c:when>
												<c:otherwise>
													<td>Inactive</td>
												</c:otherwise>
											</c:choose>

											<td><jsp:useBean id="dateObject" class="java.util.Date" />
												<jsp:setProperty name="dateObject" property="time"
													value="${list.creationDate}" /> <fmt:formatDate
													value="${dateObject}" pattern="dd/MM/yyyy hh:mm a" /></td>

											<td class="text-center">
												<!-- href="#editusermodal" data-toggle="modal" data-target="#editusermodal"  -->
												<a id="${list.id}" class="btn btn-xs btn-primary getUser"
												href="#editusermodal" data-toggle="modal"
												data-target="#editusermodal"> View </a>
											</td>
										</tr>

									</c:forEach>
								</tbody>

							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="editusermodal" class="modal fade global-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">User Detail</h4>
			</div>
			<div class="modal-body">
				<form class="modal-form change-pwd-form">
					<input type="hidden" class="usermanagementid">

					<div class="form-group clearfix">
						<label for="new_password" class="col-sm-4 control-label ">User
							Name</label>
						<div class="col-sm-8">
							<input class="form-control usermanagementusername" type="text"
								placeholder="User Name"
								onkeypress='return event.charCode==32 ||( event.charCode>=65 && event.charCode<=90) || ( event.charCode>=97 && event.charCode<=122)'
								maxlength="40" /> <span class="usermanagementusernameError"
								class="error" Style="color: red;"></span>
						</div>
					</div>
					<div class="form-group clearfix">
						<label for="email" class="col-sm-4 control-label">Email</label>
						<div class="col-sm-8">
							<input class="form-control usermanagementemail" type="email"
								readonly="readonly" placeholder="N@gmail.com" />
						</div>
					</div>
					<div class="form-group clearfix">
						<label for="bio" class="col-sm-4 control-label">Bio</label>
						<div class="col-sm-8">
							<input class="form-control usermanagementbio" type="text"
								placeholder="Bio" maxlength="100" /> <span
								class="usermanagementbioerror" class="error" Style="color: red;"></span>
						</div>
					</div>

					<div class="form-group clearfix">
						<label for="address" class="col-sm-4 control-label">Address</label>
						<div class="col-sm-8">
							<input class="form-control usermanagementaddress" type="text"
								placeholder="Address" maxlength="100" /> <span
								class="usermanagementaddresserror" class="error"
								Style="color: red;"></span>
						</div>
					</div>
					<div class="form-group clearfix">
						<label for="current_password" class="col-sm-4 control-label">Status</label>
						<div class="col-sm-8">
							<select class="form-control usermanagementuserstatus">
								<option value="active">Active</option>
								<option value="inActive">Inactive</option>
							</select>
						</div>
					</div>

					<div class="form-footer">
						<button type="button"
							class="btn btn-danger closemodalofusermanagement"
							data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success updatemodaluser">Update</button>
					</div>

				</form>
			</div>

		</div>

	</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
<script src="${path}/resources/static/js/admin/user-management.js"></script>