var $j = jQuery.noConflict();
$j(document).ready(function () {
    $j('#submit').click(function () {
        var email = $j("#email").val();
        var password = $j("#password").val();
        var emailregex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (email == "") {
            $j('#emailerr').slideDown().html("Please enter your email id.");
            return false;
        } else if (!emailregex.test(email)) {
            $j('#emailerr').slideDown().html("Please enter valid email.");
            return false;
        } else {
            clearError()

        }
        if (password == "") {
            $j('#passworderr').slideDown().html(
                "Please enter your password.");
            return false;
        } else {
            clearError()

        }

    });
});

function clearError() {
    $j('#emailerr').slideDown().html("");
    $j('#passworderr').slideDown().html("");
}
	

		
	
		