$(document).ready(function () {

    $(document).on('click', '.getUser', function () {
        var userid = $(this).attr("id");
        $(".usermanagementusernameError").html("")
        getUser(userid)

    });
    $(".updatemodaluser").click(validateUserManagementUser)
});



function validateUserManagementUser() {
    var usermanagementid = $(".usermanagementid").val();

    var usermanagementname = $(".usermanagementusername").val();

    var usermanagementemail = $(".usermanagementemail").val();

    var usermanagementbio = $(".usermanagementbio").val();

    var usermanagementaddress = $(".usermanagementaddress").val();

    var usermanagementuserstatus = $(".usermanagementuserstatus").val();

    if (usermanagementname == "") {
        $(".usermanagementusernameError").html("Please enter username.");
        return false
    } else if ((usermanagementname.length < 3) || (usermanagementname.length > 40)) {
        $(".usermanagementusernameError").html("Username must be in between 3 to 40 characters.");
        return false
    } else {
        $(".usermanagementusernameError").html("")
    }


    updateUser(usermanagementid, usermanagementname, usermanagementbio, usermanagementaddress, usermanagementuserstatus)
}

function updateUser(usermanagementid, usermanagementname, usermanagementbio, usermanagementaddress, usermanagementuserstatus) {

    $.ajax({
        type: "GET",
        url: 'updateUser',
        data: {
            "userId": usermanagementid,
            "name": usermanagementname,
            "biography": usermanagementbio,
            "address": usermanagementaddress,
            "status": usermanagementuserstatus
        },
        async: false,
        success: function (response) {

            if (response.code == "200"){
            	
            		$("#editusermodal").modal('hide');
            	   $("#success-user-update-modal").modal('show');
                   //location.reload();
            	   $('#success-user-update-modal').on('hidden.bs.modal', function () {
            		    
            		    window.location.href="userManagement";
            		 })
   
            }

        },
        error: function (e) {
            alert(JSON.stringify(e))
        }
    });

}




function getUser(userid) {
    $(".usermanagementid").val(userid);
    $.ajax({
        type: "GET",
        url: 'getUserByUserId',
        data: {
            "userId": userid
        },
        async: false,
        success: function (response) {
            $(".usermanagementusername").val(response.name);
            $(".usermanagementemail").val(response.email);
            $(".usermanagementbio").val(response.biography);
            $(".usermanagementaddress").val(response.address);

            var v;
            if (!response.status)
                v = "inActive";
            else
                v = "active";
            $('.usermanagementuserstatus option[value=' + v).attr('selected', 'selected');
        },
        error: function (e) {
        }
    });
}
