$(document).ready(function () {
    var staticContentDescription;
    var id;
    var title;
    $("#updateStaticContent").click(function () {
        staticContentDescription = $("#staticContentDescription").val();
        id = $("#id").val();
        title = $("#staticStatus").val();
        
        var state = updateStatic(staticContentDescription);
        
        if (state == true) {
            updateEnglishContent(staticContentDescription, title);
            $("#editusermodal").modal('hide');
        
        }

        	
    
    
    })

});

function getParticularStaticContent(key, title) {
    $.ajax({
        type: "get",
        contentType: "application/json",
        url: "getStaticContent?staticStatus=" + key,
        async: false,
        success: function (response) {
        	var response =  JSON.stringify(response);
        	var response=JSON.parse(response);
            $("#staticContentTitle").val(title);
            $("#staticStatus").val(key);
            $("#staticContentDescription").val(response.data);
            $("#id").val(response.data.id);
            $(".error").html("");
            $("#editusermodal").modal("show");
        },
        error: function (e) {

            console.log("error: " + e)
        }
    });
}

function updateStatic(staticContent) {

    if (staticContent == "" || staticContent == null) {
        $("#staticContentDescriptionerror").html("Please enter description.");
 
        return false;
    } else {
        $("#success-modal").modal('show');
        return true;
    }

}

function updateEnglishContent(staticContentDescription, title) {

    $.ajax({
        url: "updateStaticContent",

        /*data: {
            
            "staticContentDescription":staticContentDescription,
            
            "title":title
        },*/

        type: "GET",
        dataType: "json",
        data: {

            "staticContentDescription": staticContentDescription,

            "title": title
        },
        success: function (response) {

            $("#staticContentTitle").val(title);
            $("#staticContentDescription").val(response.data);
            $("#editstaticcontentmodal").modal("hide");
        },
        error: function (e) {
            console.log("error: " + e)
        }
    })
}