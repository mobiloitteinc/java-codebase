package com.mobiloitte.rasras.serviceimpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobiloitte.rasras.dao.NotificationDao;
import com.mobiloitte.rasras.dao.UserDao;
import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.service.NotificationService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class NotificationServiceImpl.
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);
    @Value("${pageDefaultSizeGlobal}")
    private String paginationDefaultSize;
    /**
     * The notification DAO.
     */
    @Autowired
    private NotificationDao notificationDAO;

    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.NotificationService#getNotifications(Notification, HttpServletRequest)
     */
    @Override
    public HashMap <String, Object> getNotifications (Notification notification, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getNotifications needed more information -", new ObjectMapper().writeValueAsString(notification));
        HashMap <String, Object> response = new HashMap <>();

        User userData = userDao.get(notification.getNotificationTo());
        if (userData.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }
        List <Object[]> groupList = notificationDAO.getNotifications(notification);

        if (groupList.size() > 0) {
            List <HashMap <String, Object>> notficationData = new LinkedList <>();
            for (Object[] arr : groupList) {

                HashMap <String, Object> notficationDetails = new LinkedHashMap <>();

                notficationDetails.put("notificationId", arr[0]);
                notficationDetails.put("notificationTitle", arr[1]);
                notficationDetails.put("notificationTime", arr[3]);
                notficationDetails.put("notificationDescription", arr[4]);
                notficationDetails.put("notificationType", arr[5]);
                notficationDetails.put("itemId", arr[6]);
                notficationDetails.put("offerId", arr[7]);
                notficationDetails.put("categoryId", notificationDAO.get((Integer) arr[6]).getItemCategoryId());
                notficationDetails.put("notificationUser", userDao.get((Integer) arr[2]));
                notficationData.add(notficationDetails);
            }

            Map <String, Object> paginationData = new HashMap <>();
            int totalResults = notificationDAO.getTotalResults();

            int numerator = totalResults / Integer.parseInt(paginationDefaultSize);
            int denominator = totalResults % Integer.parseInt(paginationDefaultSize);
            if (denominator > 0) {
                numerator++;
            }


            paginationData.put("totalSize", totalResults);
            paginationData.put("currentPageSize", groupList.size());
            paginationData.put("pageNumber", notificationDAO.getCurrentIndex());
            paginationData.put("totalPages", numerator);

            response.put("pagination", paginationData);
            response.put(Message.DATA, notficationData);
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NOTIFICATION_LIST_FETCHED", null, request.getLocale()));

        } else {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NO_RECORDS_FOUND", null, request.getLocale()));
            response.put(Message.DATA, null);

        }
        return response;
    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.NotificationService#setNotificationStatus(User, HttpServletRequest)
     */
    public Map <String, Object> setNotificationStatus (User user, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("setNotificationStatus needed more information -", new ObjectMapper().writeValueAsString(user));
        Map <String, Object> response = new HashMap <>();
        User userData = userDao.get(user.getUserId());
        if (userData.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }

        if (notificationDAO.setNotificationStatus(user) != 0) {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("SETTING_UPDATED", null, request.getLocale()));
            response.put(Message.DATA, null);

        } else {
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.NotificationService#getNotification(int, HttpServletRequest)
     */
    @Override
    public Map <String, Object> getNotification (int id, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getNotification needed more information - id :", +id);

        Map <String, Object> response = new HashMap <>();


        String data = notificationDAO.getNotification(id);
        if (data != null) {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NOTFICATION_DETAIL_FETCHED", null, request.getLocale()));
            response.put(Message.DATA, null);
            response.put("notificationDescription", data);
        } else if (data == null) {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NO_RECORDS_FOUND", null, request.getLocale()));
            response.put(Message.DATA, null);
        } else {
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);

        }

        return response;
    }

    @Override
    public Map <String, Object> delete (int notificationId, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("delete needed more information - id :", +notificationId);
        Map <String, Object> response = new HashMap <>();


        notificationDAO.delete(notificationId);

        response.put(Message.CODE, Code.SUCCESS);
        response.put(Message.MESSAGE, messageSource.getMessage("NOTFICATION_DELETED", null, request.getLocale()));
        response.put(Message.DATA, null);
        return response;
    }


}
