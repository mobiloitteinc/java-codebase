package com.mobiloitte.rasras.serviceimpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobiloitte.rasras.dao.ItemDao;
import com.mobiloitte.rasras.dao.UserDao;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.model.ChangePassword;
import com.mobiloitte.rasras.service.UserService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Common;
import com.mobiloitte.rasras.util.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

// TODO: Auto-generated Javadoc

/**
 * The Class UserServiceImpl.
 */
@Service
public class UserServiceImpl implements UserService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    /**
     * The b crypt password encoder.
     */
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    /**
     * The item dao.
     */
    @Autowired
    private ItemDao itemDao;

    /**
     * The common.
     */
    @Autowired
    private Common common;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.UserService#get(Integer, HttpServletRequest)
     */
    @Override
    public Map <String, Object> get (Integer userId, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getUser needed more information -", userId);
        Map <String, Object> response = new HashMap <>();
        if (!userDao.get(userId).getStatus()) {
            LOGGER.warn("user is blocked-", userId);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }

        User user = userDao.getUserWithItem(userId);
        Set <Item> itemList = user.getItems();
        List <Image> imageList;
        Set <Item> myItemList = new HashSet <>();
        for (Item items : itemList) {
            imageList = itemDao.getItemImages(items.getItemId());
            for (Image image : imageList) {
                if (image.getCoverPic()) {
                    items.setItemImage(image);
                    break;
                }
            }
            if (items.getDeleteStatus() == 0)
                myItemList.add(items);

        }
        user.setItems(myItemList);


        response.put(Message.CODE, Code.SUCCESS);
        response.put(Message.MESSAGE, messageSource.getMessage("USER_FETCHED", null, request.getLocale()));
        response.put(Message.DATA, user);

        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.UserService#change(ChangePassword, HttpServletRequest)
     */
    @Override
    public Map <String, Object> change (ChangePassword changePassword, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("change needed more information -", new ObjectMapper().writeValueAsString(changePassword));

        Map <String, Object> response = new HashMap <>();
        User user = userDao.get(changePassword.getUserId());

        if (user.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }


        int updateStatus = 0;

        if (bCryptPasswordEncoder.matches(changePassword.getCurrentPassword(), user.getPassword())) {
            changePassword.setNewPassword(bCryptPasswordEncoder.encode(changePassword.getNewPassword()));
            updateStatus = userDao.change(changePassword);
        }
        if (updateStatus > 0) {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("PASSWORD_CHANGED_SUCCESSFULLY", null, request.getLocale()));
            response.put(Message.DATA, null);
        } else {
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("OLD_PASSWORD_WRONG", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.UserService#update(UserDetails, HttpServletRequest)
     */
    @Override
    public Map <String, Object> update (UserDetails userDetails, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("update needed more information -", new ObjectMapper().writeValueAsString(userDetails));
        Map <String, Object> response = new HashMap <>();

        User user = userDao.get(userDetails.getUserDetailsId());

        if (user.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }


        if (userDetails.getImage().equals("")) {

            userDetails.setImage(user.getUserDetails().getImage());
        } else {
            userDetails.setImage(common.upload(userDetails.getImage()));
        }

        UserDetails userDetails1 = userDao.update(userDetails);
        response.put(Message.CODE, Code.SUCCESS);
        response.put(Message.MESSAGE, messageSource.getMessage("USER_INFORMATION_UPDATED_SUCCESSFULLY", null, request.getLocale()));
        response.put(Message.DATA, userDetails1);
        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.UserService#verify(OTP, HttpServletRequest)
     */
    @Override
    public Map <String, Object> verify (OTP otp, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("verify needed more information -", new ObjectMapper().writeValueAsString(otp));
        Map <String, Object> response = new HashMap <>();


        User user = userDao.get(otp.getUserId());

        if (user.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }


        if (userDao.verify(otp) != null) {
            LOGGER.info("OTP matched - ", new ObjectMapper().writeValueAsString(otp));
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("OTP_MATHCHED", null, request.getLocale()));
            response.put(Message.DATA, null);
        } else {
            LOGGER.warn("wrong OTP entered - ", new ObjectMapper().writeValueAsString(otp));
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("WRONG_OTP", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.UserService#resetPassword(ChangePassword, HttpServletRequest)
     */
    @Override
    public Map <String, Object> resetPassword (ChangePassword changePassword, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("Reset password needed more information -", new ObjectMapper().writeValueAsString(changePassword));
        Map <String, Object> response = new HashMap <>();

        User user = userDao.get(changePassword.getUserId());
        if (user.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }


        changePassword.setNewPassword(bCryptPasswordEncoder.encode(changePassword.getNewPassword()));
        if (userDao.change(changePassword) > 0) {
            LOGGER.info("Password Updated successfully");
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("PASSWORD_CHANGED_SUCCESSFULLY", null, request.getLocale()));
            response.put(Message.DATA, null);
        } else {
            LOGGER.info("Unable to update password");
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

}
