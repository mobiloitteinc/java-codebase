package com.mobiloitte.rasras.serviceimpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobiloitte.rasras.dao.ItemDao;
import com.mobiloitte.rasras.dao.UserDao;
import com.mobiloitte.rasras.entity.Comment;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.Like;
import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.Offer;
import com.mobiloitte.rasras.entity.OfferItems;
import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.service.ItemService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Common;
import com.mobiloitte.rasras.util.Message;
import com.mobiloitte.rasras.util.PushNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class ItemServiceImpl.
 */
@Service
public class ItemServiceImpl implements ItemService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemServiceImpl.class);

    /**
     * The item dao.
     */
    @Autowired
    private ItemDao itemDao;


    @Value("${pageDefaultSizeGlobal}")
    private String paginationDefaultSize;

    @Value("${itemSize}")
    private String itemSize;
    /**
     * The common.
     */
    @Autowired
    private Common common;

    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    /**
     * The push notification.
     */
    @Autowired
    private PushNotification pushNotification;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * The message dao.
     */
    @Autowired
    private MessageDao messageDao;

    @Autowired
    private OfferDao offerDao;

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#save(Item, HttpServletRequest)
     */
    @Override
    public Map <String, Object> save (Item item, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("save item needed more information -" + new ObjectMapper().writeValueAsString(item));
        Map <String, Object> response = new HashMap <>();
        if (!userDao.get(item.getUserItemId()).getStatus()) {
            LOGGER.warn("user is blocked-", item.getUserItemId());
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }
        UserDetails userDetails = userDao.get((int) item.getUserItemId());
        item.setLatitude(userDetails.getLatitude());
        item.setLongitude(userDetails.getLongitude());
        List <Image> itemImages = new ArrayList <>();
        for (Image image : item.getItemImages()) {
            String imageUrl = common.upload(image.getBase64data());
            image.setImageUrl(imageUrl);
            itemImages.add(image);
        }
        item.setItemImages(itemImages);


        int result = itemDao.save(item);
        if (result > 0) {
            item.setItemId(result);


            item.setDislikeByCurrentUser(false);
            item.setLikeByCurrentUser(false);
            response.put(Message.DATA, item);
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("ITEM_SAVED_SUCCESSFULLY", null, request.getLocale()));
        } else {
            LOGGER.warn("save needed to warn -", result);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("UNABLE_TO_SAVE_ITEM", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#getItem(HttpServletRequest)
     */
    public Map <String, Object> getItem (HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getItem needed more information -");
        Map <String, Object> response = new HashMap <>();


        List <Object> dataList = new ArrayList <>();
        Map <String, Object> dataMap = new HashMap <>();
        List <Category> categoryList = itemDao.getCategory();
        dataMap.put("category", categoryList);


        dataList.add(dataMap);

        response.put(Message.CODE, Code.SUCCESS);
        response.put(Message.MESSAGE, messageSource.getMessage("ITEM_RETRIEVED_SUCCESSFULLY", null, request.getLocale()));
        response.put(Message.DATA, dataList);
        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#getItem(int, HttpServletRequest)
     */
    public Map <String, Object> getItem (int categoryId, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getItem needed more information -");
        Map <String, Object> response = new HashMap <>();
        List <Item> itemList = itemDao.getItem(categoryId);

        if (!itemList.isEmpty()) {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("ITEM_RETRIEVED_SUCCESSFULLY", null, request.getLocale()));
            response.put(Message.DATA, itemList);
        } else {
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("UNABLE_TO_RETRIEVE_ITEM", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#addLike(Like, HttpServletRequest)
     */
    @Override
    public Map <String, Object> addLike (Like like, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("addLike needed more information -");
        Map <String, Object> response = new HashMap <>();
        User userData = userDao.get(like.getLikedBy());
        if (userData.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }

        Like likeStatus = itemDao.isLiked(like.getItemLikeId(), like.getLikedBy());
        if (likeStatus != null) {
            itemDao.unlike(like.getLikedBy(), like.getItemLikeId());
            itemDao.subtractLikeCount(like.getItemLikeId());


            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("UNABLE_TO_SET_FAVOURITE", null, request.getLocale()));
            Map <String, Object> status = new HashMap <>();
            status.put("isFavourite", false);
            response.put(Message.DATA, status);
            return response;
        } else {
            itemDao.addLike(like);
            itemDao.addLikeCount(like.getItemLikeId());
            Notification notficationInfo = itemDao.saveNotificationOnLike(like);
            if (notficationInfo != null) {
                //* This is a block to send push notfication to android and apple devices using FCM and APNS libraries.*//*
                Map <String, Object> data = new HashMap <>();
                data.put("title", notficationInfo.getNotificationTitle());
                data.put("body", notficationInfo.getNotificationDescription());
                data.put("fromUserId", like.getLikedBy());
                data.put("toUserId", notficationInfo.getNotificationTo());
                data.put("itemId", like.getItemLikeId());
                int categoryId = itemDao.getCategoryIdFromItem(like.getItemLikeId());
                data.put("categoryId", categoryId);
                data.put("categoryTitle", itemDao.getCategory(categoryId).getCategoryName());
                data.put("type", "like");
                data.put("data", null);
                pushNotification.sendPushNotification(data);
                //* Push notifciation code ends here.*//*

            }
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("FAVOURITE_SET_SUCCESSFULLY", null, request.getLocale()));
            Map <String, Object> status = new HashMap <>();
            status.put("isFavourite", true);
            response.put(Message.DATA, status);
            return response;
        }


    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#addComment(Comment, HttpServletRequest)
     */
    public Map <String, Object> addComment (Comment comment, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("addComment needed more information -");
        Map <String, Object> response = new HashMap <>();

        User userData = userDao.get(comment.getCommentBy());
        if (userData.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }

        int result = itemDao.addComment(comment);
        if (result > 0) {
            comment.setCommentId(result);
            itemDao.addCommentCount(comment.getItemCommentId());
            Notification notficationInfo = itemDao.saveNotificationOnComment(comment);

            if (notficationInfo != null) {    /* This is a block to send push notfication to android and apple devices using FCM and APNS libraries.*/
                Map <String, Object> data = new HashMap <>();
                data.put("title", notficationInfo.getNotificationTitle());
                data.put("body", notficationInfo.getNotificationDescription());
                data.put("fromUserId", comment.getCommentBy());
                data.put("toUserId", notficationInfo.getNotificationTo());
                data.put("itemId", comment.getItemCommentId());
                int categoryId = itemDao.getCategoryIdFromItem(comment.getItemCommentId());
                data.put("categoryId", categoryId);
                data.put("categoryTitle", itemDao.getCategory(categoryId).getCategoryName());
                data.put("type", "comment");
                data.put("data", null);
                pushNotification.sendPushNotification(data);
               /* Push notifciation code ends here.*/

            }
            Map <String, Object> commentDetails = new HashMap <>();
            commentDetails.put("comment", comment);
            commentDetails.put("user", userDao.get(comment.getCommentBy()));

            response.put(Message.DATA, commentDetails);
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("COMMENT_ADDED_SUCCESSFULLY", null, request.getLocale()));

        } else {
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("UNABLE_TO_COMMENT", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#getItemDetail(Item, HttpServletRequest)
     */
    public Map <String, Object> getItemDetail (Item item, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getItemDetail needed more information -");
        Map <String, Object> response = new HashMap <>();
        Map itemDetail = itemDao.getItemDetail(item);
        Like itemLike = itemDao.isLiked((int) itemDetail.get("itemId"), item.getUserItemId());
        User user = userDao.get((Integer) itemDetail.get("userItemId"));

        if (user.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }


        if (itemLike != null) {
            itemDetail.put("isFavourite", true);
        } else {
            itemDetail.put("isFavourite", false);
        }
        boolean isOffered = false;

        Offer itemStatus = itemDao.isOffered(item.getUserItemId(), item.getItemId(), (Integer) itemDetail.get("userItemId"));

        if (itemStatus != null) {
            isOffered = true;
        }

        itemDetail.put("isOffered", isOffered);
        itemDetail.put("name", user.getUserDetails().getFirstName() + " " + user.getUserDetails().getLastName());
        itemDetail.put("image", user.getUserDetails().getImage());
        itemDetail.put("itemImages", itemDao.getItemImages((int) itemDetail.get("itemId")));


        response.put(Message.DATA, itemDetail);
        response.put(Message.CODE, Code.SUCCESS);
        response.put(Message.MESSAGE, messageSource.getMessage("ITEM_RETRIEVED_SUCCESSFULLY", null, request.getLocale()));

        return response;
    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#getComments(int, HttpServletRequest, int)
     */
    public Map <String, Object> getComments (int itemId, HttpServletRequest request, int pageNumber) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getComments needed more information -");
        Map <String, Object> response = new HashMap <>();


        List <Comment> commentList = itemDao.getComments(itemId, pageNumber);
        List <Comment> commentList2 = new LinkedList <>();
        if (!commentList.isEmpty()) {
            for (Comment comment1 : commentList) {
                User user = userDao.get(comment1.getCommentBy());
                comment1.setUser(user.getUserDetails());
                commentList2.add(comment1);
            }
            Map <String, Object> paginationMap = new HashMap <>();
            int totalResults = itemDao.getAllActivitySize();


            int numerator = totalResults / Integer.parseInt(paginationDefaultSize);
            int denominator = totalResults % Integer.parseInt(paginationDefaultSize);
            if (denominator > 0) {
                numerator++;
            }


            paginationMap.put("totalSize", totalResults);
            paginationMap.put("currentPageSize", commentList.size());
            paginationMap.put("pageNumber", itemDao.getCurrentIndex());

            Collections.reverse(commentList2);

            paginationMap.put("totalPages", numerator);
            response.put("pagination", paginationMap);
            response.put(Message.DATA, commentList2);
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("COMMENT_RETRIEVED_SUCCESSFULLY", null, request.getLocale()));
        } else {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("UNABLE_TO_RETRIEVE_COMMENT", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#getHomeItems(Item, HttpServletRequest)
     */
    @Override
    public Map <String, Object> getHomeItems (Item item, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getHomeItems needed more information -");

        Map <String, Object> response = new HashMap <>();
        User userData = userDao.get(item.getUserItemId());
        if (userData.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }
        List <Map <String, Object>> homeList = itemDao.getHomeItems(item);

        List <Map <String, Object>> homeList1 = new ArrayList <>();
        if (!homeList.isEmpty()) {

            for (Map <String, Object> itemMap : homeList) {
                List <Image> imageList = itemDao.getItemImages((int) itemMap.get("itemId"));

                for (Image coverImage : imageList) {
                    if (coverImage.getCoverPic()) {
                        itemMap.put("itemImages", coverImage);
                        break;
                    }
                }
                User user = userDao.get((Integer) itemMap.get("userItemId"));
                Like itemLike = itemDao.isLiked((int) itemMap.get("itemId"), item.getUserItemId());

                if (itemLike != null) {
                    itemMap.put("isFavourite", true);
                } else {
                    itemMap.put("isFavourite", false);
                }
                itemMap.put("name", user.getUserDetails().getFirstName() + " " + user.getUserDetails().getLastName());
                itemMap.put("image", user.getUserDetails().getImage());
                itemMap.put("chatCount", messageDao.getCount((int) itemMap.get("itemId")));
                homeList1.add(itemMap);

            }
            Map <String, Object> paginationMap = new HashMap <>();
            int totalResults = itemDao.getAllActivitySize();


            int numerator = totalResults / Integer.parseInt(itemSize);
            int denominator = totalResults % Integer.parseInt(itemSize);
            if (denominator > 0) {
                numerator++;
            }


            paginationMap.put("totalSize", totalResults);
            paginationMap.put("currentPageSize", homeList.size());
            paginationMap.put("pageNumber", itemDao.getCurrentIndex());

            paginationMap.put("totalPages", numerator);
            response.put("pagination", paginationMap);


            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("ITEM_RETRIEVED_SUCCESSFULLY", null, request.getLocale()));
            response.put(Message.DATA, homeList1);
        } else {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NO_RECORDS_FOUND", null, request.getLocale()));
        }

        return response;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#getFavouriteItems(Integer, HttpServletRequest, int)
     */
    @Override
    public Map <String, Object> getFavouriteItems (Item item, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getFavouriteItems needed more information -");
        Map <String, Object> response = new HashMap <>();

        User userData = userDao.get(item.getUserItemId());
        if (userData.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }
        List <Map <String, Object>> itemList = itemDao.getFavouriteItems(item);

        List <Map <String, Object>> itemList1 = new ArrayList <>();
        if (!itemList.isEmpty()) {

            for (Map <String, Object> itemMap : itemList) {
                List <Image> imageList = itemDao.getItemImages((int) itemMap.get("itemId"));

                for (Image coverImage : imageList) {
                    if (coverImage.getCoverPic()) {
                        itemMap.put("itemImages", coverImage);
                        break;
                    }
                }
                User user = userDao.get((Integer) itemMap.get("userItemId"));
                Like itemLike = itemDao.isLiked((int) itemMap.get("itemId"), item.getUserItemId());

                if (itemLike != null) {
                    itemMap.put("isFavourite", true);
                } else {
                    itemMap.put("isFavourite", false);
                }
                itemMap.put("name", user.getUserDetails().getFirstName() + " " + user.getUserDetails().getLastName());
                itemMap.put("image", user.getUserDetails().getImage());
                itemList1.add(itemMap);

            }
            Map <String, Object> paginationMap = new HashMap <>();
            int totalResults = itemDao.getAllActivitySize();


            int numerator = totalResults / Integer.parseInt(itemSize);
            int denominator = totalResults % Integer.parseInt(itemSize);
            if (denominator > 0) {
                numerator++;
            }


            paginationMap.put("totalSize", totalResults);
            paginationMap.put("currentPageSize", itemList.size());
            paginationMap.put("pageNumber", itemDao.getCurrentIndex());

            paginationMap.put("totalPages", numerator);
            response.put("pagination", paginationMap);


            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("FAVOURITE_RETRIEVED_SUCCESSFULLY", null, request.getLocale()));
            response.put(Message.DATA, itemList1);
        } else {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NO_RECORDS_FOUND", null, request.getLocale()));
        }

        return response;

    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.service.ItemService#search(String, Item, HttpServletRequest)
     */
    @Override
    public Map <String, Object> search (Item item, HttpServletRequest request) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("search needed more information -");
        Map <String, Object> response = new HashMap <>();
        User userData = userDao.get(item.getUserItemId());
        if (userData.getStatus().equals(false)) {
            response.put(Message.CODE, Code.BLOCKED);
            response.put(Message.MESSAGE, messageSource.getMessage("USER_IS_BLOCKED", null, request.getLocale()));
            response.put(Message.DATA, null);
            return response;
        }

        List <Map <String, Object>> homeList = itemDao.search(item.getItemTitle(), item);

        List <Map <String, Object>> homeList1 = new ArrayList <>();
        if (!homeList.isEmpty()) {

            for (Map <String, Object> itemMap : homeList) {
                List <Image> imageList = itemDao.getItemImages((int) itemMap.get("itemId"));

                for (Image coverImage : imageList) {
                    if (coverImage.getCoverPic()) {
                        itemMap.put("itemImages", coverImage);
                        break;
                    }
                }
                User user = userDao.get((Integer) itemMap.get("userItemId"));
                Like itemLike = itemDao.isLiked((int) itemMap.get("itemId"), item.getUserItemId());

                if (itemLike != null) {
                    itemMap.put("isFavourite", true);
                } else {
                    itemMap.put("isFavourite", false);
                }
                itemMap.put("name", user.getUserDetails().getFirstName() + " " + user.getUserDetails().getLastName());
                itemMap.put("image", user.getUserDetails().getImage());
                homeList1.add(itemMap);

            }
            Map <String, Object> paginationMap = new HashMap <>();
            int totalResults = itemDao.getAllActivitySize();


            int numerator = totalResults / Integer.parseInt(itemSize);
            int denominator = totalResults % Integer.parseInt(itemSize);
            if (denominator > 0) {
                numerator++;
            }


            paginationMap.put("totalSize", totalResults);
            paginationMap.put("currentPageSize", homeList.size());
            paginationMap.put("pageNumber", itemDao.getCurrentIndex());

            paginationMap.put("totalPages", numerator);
            response.put("pagination", paginationMap);


            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("ITEM_RETRIEVED_SUCCESSFULLY", null, request.getLocale()));
            response.put(Message.DATA, homeList1);
        } else {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NO_RECORDS_FOUND", null, request.getLocale()));
        }

        return response;

    }


    //@Transactional
    public Map <String, Object> deleteItem (int itemId, HttpServletRequest request) throws Exception {
        Map <String, Object> response = new HashMap <>();
        Offer offer = new Offer();
        offer.setOfferOnItemId(itemId);

        List <Offer> offers = offerDao.getOffers(itemId);

        if (!offers.isEmpty()) {
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("ITEM_CAN_NOT_DELETED", null, request.getLocale()));
            return response;
        }


        offerDao.deleteOffer(itemId);
        int result = offerDao.deleteItem(offer);
        if (result > 0) {

            List <OfferItems> offerItemsList = offerDao.getOfferItems(itemId);

            for (OfferItems offerItems : offerItemsList) {

                try {
                    offerDao.isSingleOfferOnItem(offerItems.getOfferId());
                    int status = offerDao.deleteOfferWithOfferId(offerItems.getOfferId());
                    if (status == 0) {
                        offerDao.rollbackDeleteItem(offer);
                        offerDao.rollbackDeleteOffer(itemId);
                        response.put(Message.CODE, Code.FAILURE);
                        response.put(Message.MESSAGE, messageSource.getMessage("ITEM_CAN_NOT_DELETED", null, request.getLocale()));
                        return response;
                    }
                } catch (Exception e) {
                    LOGGER.error("exception  is: " + e);
                    offerDao.deleteItemFromOfferItems(offerItems.getOfferItemId());
                }
            }


            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("ITEM_DELETED_SUCCESSFULLY", null, request.getLocale()));
        } else {
            response.put(Message.CODE, Code.SUCCESS);
            response.put(Message.MESSAGE, messageSource.getMessage("NO_RECORDS_FOUND", null, request.getLocale()));
        }

        return response;
    }
}
