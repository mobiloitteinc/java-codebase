package com.mobiloitte.rasras.rest.controller;


import com.mobiloitte.rasras.entity.Offer;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class OfferController.
 */
@RestController
@RequestMapping("rest")
@Api(value = "Offer", description = "User offer operations are performed here.")
class OfferController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OfferController.class);

    /**
     * The offer service.
     */
    @Autowired
    private OfferService offerService;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Gets the my offer list.
     *
     * @param userId
     *         the user id
     * @param request
     *         the request
     * @return the map
     */
    @GetMapping("getMyOffer/{userId}")
    @ApiOperation(value = "get available offers  from this controller")
    public Map <String, Object> get (@PathVariable("userId") Integer userId, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();

        try {
            response = offerService.getMyOffer(userId, request);

        } catch (Exception e) {
            LOGGER.error("get offers encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /**
     * Adds the new offer.Can not add offer when offer is provided already to a user with the same item.
     *
     * @param offer
     *         the offer
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("addOffer")
    public Map <String, Object> addOffer (@RequestBody Offer offer, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = offerService.save(offer, request);

        } catch (Exception e) {

            LOGGER.error("save offers encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Gets the offer from any other user.
     *
     * @param offer
     *         the offer
     * @param request
     *         the request
     * @return the offer from user
     */
    @PostMapping("getOfferFromUser")
    public Map <String, Object> getOfferFromUser (@RequestBody Offer offer, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = offerService.getOfferFromUser(offer, request);
        } catch (Exception e) {
            LOGGER.error("get offers from user encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Gets the offer details of a single offer.
     *
     * @param offer
     *         the offer
     * @param request
     *         the request
     * @return the offer details
     */
    @PostMapping("getOfferDetails")
    public Map <String, Object> getOfferDetails (@RequestBody Offer offer, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = offerService.getMyOfferDetails(offer, request);
        } catch (Exception e) {
            LOGGER.error("offer details encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Updates the deal as completed.
     *
     * @param offer
     *         the offer
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("doneDeal")
    public Map <String, Object> doneDeal (@RequestBody Offer offer, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();

        try {
            response = offerService.doneDeal(offer, request);

        } catch (HibernateException e) {
            LOGGER.error("Done deal encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("Done deal encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


    /**
     * Updates the deal status.
     *
     * @param offer
     *         the offer
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("updateDealStatus")
    public Map <String, Object> updateDealStatus (@RequestBody Offer offer, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = offerService.updateDealStatus(offer, request);
        } catch (Exception e) {
            LOGGER.error("Update deal encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Delete my posted deals(permanent delete from  the database).
     *
     * @param offer
     *         the offer
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("deleteOffer")
    public Map <String, Object> delete (@RequestBody Offer offer, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = offerService.delete(offer, request);
        } catch (Exception e) {
            LOGGER.error("delete deal encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }
}
