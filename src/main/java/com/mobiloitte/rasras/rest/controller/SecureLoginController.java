package com.mobiloitte.rasras.rest.controller;


import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.model.ChangePassword;
import com.mobiloitte.rasras.service.UserService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


// TODO: Auto-generated Javadoc

/**
 * The Class SecureLoginController.
 */
@RestController
@RequestMapping("security")
@Api(value = "User", description = "User login operations are performed here.")
public class SecureLoginController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SecureLoginController.class);


    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * The login service.
     */
    @Autowired
    private LoginService loginService;

    /**
     * The user service.
     */
    @Autowired
    private UserService userService;

    /**
     * The static service.
     */
    @Autowired
    private StaticService staticService;


    /**
     * Gets the static content added from the admin panel.
     *
     * @param staticContent
     *         the static content
     * @param request
     *         the request
     * @return the content
     */
    @PostMapping("staticContent")
    public Map <String, Object> getContent (@RequestBody StaticContent staticContent, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = staticService.getContent(staticContent, request);
        } catch (HibernateException e) {
            LOGGER.error("getContent encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getContent encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * User signup for account from here.
     *
     * @param user
     *         the user
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("saveUser")
    @ApiOperation(value = "saving user from this controller")
    public Map <String, Object> signup (@Valid @RequestBody User user, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            Role role = new Role();
            role.setRole("user");
            Set <Role> roleSet = new HashSet <>();
            roleSet.add(role);
            user.setRole(roleSet);
            response = loginService.save(user, request);

        } catch (MethodArgumentNotValidException e) {
            LOGGER.error("signup encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, e.getBindingResult());
            response.put(Message.DATA, null);
            e.printStackTrace();
        } catch (DataIntegrityViolationException e) {
            LOGGER.error("signup encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("EMAIL_MOBILE_EXISTS", null, request.getLocale()));
            response.put(Message.DATA, null);
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.error("signup encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
            e.printStackTrace();
        }

        return response;
    }

    /**
     * User can login from here.
     *
     * @param user
     *         the user
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("login")
    @ApiOperation(value = "login from this controller")
    public Map <String, Object> login (@RequestBody User user, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {


            response = loginService.login(user, request);

        } catch (NullPointerException e) {
            LOGGER.error("login encountered an warning -", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("ENTER_VALID_CREDENTIALS", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("login encountered an error - ", e);

            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /**
     * User can reset the password when he/she foregt.
     *
     * @param user
     *         the user
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("forgotPassword")
    @ApiOperation(value = "forgot password from this controller")
    public Map <String, Object> forgotPassword (@RequestBody User user, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = loginService.forgot(user, request);
        } catch (HibernateException e) {
            LOGGER.error("forgotPassword encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("forgotPassword encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


    /**
     * User can reset his/her password from here.
     *
     * @param changePassword
     *         the change password
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("resetPassword")
    @ApiOperation(value = "Reset password from this controller")
    public Map <String, Object> resetPassword (@RequestBody ChangePassword changePassword, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = userService.resetPassword(changePassword, request);
        } catch (HibernateException e) {
            LOGGER.error("Reset password encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("Reset password encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * User can resend OTP from this controller.
     *
     * @param userId
     *         the user id
     * @param request
     *         the request
     * @return the map
     */
    @GetMapping("resendOTP/{userId}")
    @ApiOperation(value = "resend OTP from this controller")
    public Map <String, Object> resendOTP (@PathVariable("userId") Integer userId, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = loginService.updateOtp(userId, request);
        } catch (HibernateException e) {
            LOGGER.error("resendOTP encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("resendOTP encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * User can resend OTP from this controller.
     *
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("updateLanguage")
    @ApiOperation(value = "update language from this controller")
    public Map <String, Object> updateLanguage (@RequestBody User user, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = loginService.updateLanguage(user, request);
        } catch (HibernateException e) {
            LOGGER.error("updateLanguage encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("updateLanguage encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


}


