package com.mobiloitte.rasras.rest.controller;


import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


// TODO: Auto-generated Javadoc

/**
 * The Class LoginController.
 */
@RestController
@RequestMapping("rest")
@Api(value = "User", description = "User login operations are performed here.")
class LoginController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    /**
     * The login service.
     */
    @Autowired
    private LoginService loginService;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Logout from the device.
     *
     * @param user
     *         the user
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("logout")
    @ApiOperation(value = "logout from this controller")
    public Map <String, Object> logout (@RequestBody User user, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = loginService.logout(user, request);
        } catch (HibernateException e) {
            LOGGER.error("logout encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("logout encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;

    }


}
