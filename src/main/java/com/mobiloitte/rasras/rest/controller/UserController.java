package com.mobiloitte.rasras.rest.controller;

import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.model.ChangePassword;
import com.mobiloitte.rasras.service.UserService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class UserController.
 */
@RestController
@RequestMapping("rest")
@Api(value = "Profile", description = "User profile operations are performed here.")
class UserController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    /**
     * The user service.
     */
    @Autowired
    private UserService userService;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Gets the profile of a user.
     *
     * @param userId
     *         the user id
     * @param request
     *         the request
     * @return the map
     */
    @GetMapping("profile/{userId}")
    @ApiOperation(value = "get user profile from this controller")
    public Map <String, Object> get (@PathVariable("userId") Integer userId, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {


            response = userService.get(userId, request);
        } catch (HibernateException e) {
            LOGGER.error("getUser encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getUser encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /**
     * Change password of the user from here.
     *
     * @param changePassword
     *         the change password
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("changePassword")
    public Map <String, Object> change (@RequestBody ChangePassword changePassword, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = userService.change(changePassword, request);
        } catch (HibernateException e) {
            LOGGER.error("change encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("change encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * A user can update his/her profile from here.
     *
     * @param userDetails
     *         the user details
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("edit")
    @ApiOperation(value = "update user profile from this controller")
    public Map <String, Object> update (@RequestBody UserDetails userDetails, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = userService.update(userDetails, request);
        } catch (HibernateException e) {
            LOGGER.error("update encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("update encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Verify the OTP provided by the user.
     *
     * @param otp
     *         the otp
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("verifyOTP")
    @ApiOperation(value = "verify OTP from this controller")
    public Map <String, Object> verify (@RequestBody OTP otp, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = userService.verify(otp, request);
        } catch (HibernateException e) {
            LOGGER.error("verify encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("verify encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


}
