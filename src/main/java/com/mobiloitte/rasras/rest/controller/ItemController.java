package com.mobiloitte.rasras.rest.controller;

import com.mobiloitte.rasras.entity.Comment;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.Like;
import com.mobiloitte.rasras.service.ItemService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class ItemController.
 */
@RestController
@RequestMapping("rest")
@Api(value = "Item", description = "Item operations are performed here.")
class ItemController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);


    /**
     * The item service.
     */
    @Autowired
    private ItemService itemService;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Controller to save the item.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("saveItem")
    @ApiOperation(value = "saving items from this controller")
    public Map <String, Object> save (@RequestBody Item item, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = itemService.save(item, request);
        } catch (DataIntegrityViolationException e) {
            LOGGER.error("save item encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("save item encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /**
     * Gets the item category
     *
     * @param request
     *         the request
     * @return the item
     */
    @GetMapping(value = "getItemCategoryColorSizeList")
    public Map <String, Object> getItem (HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {

            response = itemService.getItem(request);
        } catch (HibernateException e) {
            LOGGER.error("getItem encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getItem encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


    /**
     * Gets the item based on the category.
     *
     * @param categoryId
     *         the category id
     * @param request
     *         the request
     * @return the item
     */
    @GetMapping(value = "getItemBasedOnCategory/{categoryId}")
    public Map <String, Object> getItem (@PathVariable("categoryId") Integer categoryId, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = itemService.getItem(categoryId, request);
        } catch (HibernateException e) {
            LOGGER.error("getItem encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getItem encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        System.out.println("Response from controller " + response);
        return response;
    }

    /**
     * Adds the favourite item in the user profile
     *
     * @param request
     *         the request
     * @return the map
     */

    @GetMapping(value = "deleteItem/{itemId}")
    public Map <String, Object> deleteItem (@PathVariable("itemId") Integer itemId, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = itemService.deleteItem(itemId, request);
        } catch (Exception e) {
            LOGGER.error("deleteItem encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
        }
        return response;
    }

    @PostMapping(value = "addFavourite")
    public Map <String, Object> addLike (@RequestBody Like like, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = itemService.addLike(like, request);

        } catch (HibernateException e) {
            LOGGER.error("addLike encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("addLike encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


    /**
     * Adds the comment in a specific item.
     *
     * @param comment
     *         the comment
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping(value = "addComment")
    public Map <String, Object> addComment (@RequestBody Comment comment, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = itemService.addComment(comment, request);
        } catch (HibernateException e) {
            LOGGER.error("addComment encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("addComment encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Gets the item detail.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the item detail
     */
    @PostMapping(value = "getItemDetail")
    public Map <String, Object> getItemDetail (@RequestBody Item item, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();

        try {
            response = itemService.getItemDetail(item, request);
        } catch (HibernateException e) {
            LOGGER.error("getItemDetail encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getItemDetail encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Gets the comments of an item.
     *
     * @param itemId
     *         the item id
     * @param request
     *         the request
     * @param pageNumber
     *         the page number
     * @return the comments
     */
    @GetMapping(value = "showComment/{itemId}/{pageNumber}")
    public Map <String, Object> getComments (@PathVariable("itemId") int itemId, HttpServletRequest request, @PathVariable("pageNumber") int pageNumber) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = itemService.getComments(itemId, request, pageNumber);
        } catch (HibernateException e) {
            LOGGER.error("getComments encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getComments encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Gets the home items according to the latitude and longitude provided from the device.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the home items
     */
    @PostMapping("homeList")
    public Map <String, Object> getHomeItems (@RequestBody Item item, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {

            response = itemService.getHomeItems(item, request);

        } catch (DataIntegrityViolationException e) {
            LOGGER.error("getHomeItems encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getHomeItems  encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }

        return response;
    }

    /**
     * Gets the favourite items of a user.
     *
     * @param request
     *         the request
     * @return the favourite items
     */
    @PostMapping("favourite")
    public Map <String, Object> getFavouriteItems (@RequestBody Item item, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = itemService.getFavouriteItems(item, request);
        } catch (HibernateException e) {
            LOGGER.error("getFavouriteItems encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getFavouriteItems encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;

    }

    /**
     * Search the items according to the latitude and longitude provided from the device.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("search")
    public Map <String, Object> search (@RequestBody Item item, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {


            response = itemService.search(item, request);
        } catch (HibernateException e) {
            LOGGER.error("search encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("search encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


}
