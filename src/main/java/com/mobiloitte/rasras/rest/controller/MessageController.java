package com.mobiloitte.rasras.rest.controller;

import com.mobiloitte.rasras.entity.Messages;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import io.swagger.annotations.Api;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class MessageController.
 */
@RestController
@RequestMapping("rest")
@Api(value = "Message", description = "Message operations are performed here.")
class MessageController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);

    /**
     * The message service.
     */
    @Autowired
    private MessageService messageService;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Send message to the user from item or direct to the user.
     *
     * @param message
     *         the message
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("sendMessage")
    public Map <String, Object> send (@RequestBody Messages message, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = messageService.send(message, request);
        } catch (HibernateException e) {
            LOGGER.error("send message encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
        } catch (Exception e) {
            LOGGER.error("send message encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
        }
        return response;

    }

    /**
     * Gets the message list of user.
     *
     * @param message
     *         the message
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("getMessage")
    public Map <String, Object> get (@RequestBody Messages message, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {

            response = messageService.get(message, request);
        } catch (HibernateException e) {
            LOGGER.error("get message encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
        } catch (Exception e) {
            LOGGER.error("get message encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
        }
        return response;
    }

    /**
     * Gets the user one to one message thread.
     *
     * @param message
     *         the message
     * @param request
     *         the request
     * @return the user
     */
    @PostMapping("getUserMessage")
    public Map <String, Object> getUser (@RequestBody Messages message, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {

            response = messageService.getUser(message, request);
        } catch (HibernateException e) {
            LOGGER.error("get message encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
        } catch (Exception e) {
            LOGGER.error("get message encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
        }
        return response;
    }
}
