package com.mobiloitte.rasras.rest.controller;

import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.service.NotificationService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class NotificationController.
 */
@RestController
@RequestMapping("rest")
class NotificationController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

    /**
     * The notification service.
     */
    @Autowired
    private NotificationService notificationService;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Gets all the notifications received by the logged in user.
     *
     * @param notification
     *         the notification
     * @param request
     *         the request
     * @return the notifications
     */
    @PostMapping(value = "getAllNotification")
    public Map <String, Object> getNotifications (@RequestBody Notification notification, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = notificationService.getNotifications(notification, request);
        } catch (HibernateException e) {
            LOGGER.error("getNotifications encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getNotifications encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Sets the notification status to on andoff mode.
     *
     * @param user
     *         the user
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping(value = "setNotificationStatus")
    public Map <String, Object> setNotificationStatus (@RequestBody User user, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = notificationService.setNotificationStatus(user, request);
        } catch (HibernateException e) {
            LOGGER.error("setNotificationStatus encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("setNotificationStatus encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }


    /**
     * Gets the notification description of a single notification.
     *
     * @param notification
     *         the notification
     * @param request
     *         the request
     * @return the notification
     */
    @PostMapping(value = "getSingleNotification")
    public Map <String, Object> getNotification (@RequestBody Notification notification, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = notificationService.getNotification(notification.getNotificationId(), request);
        } catch (HibernateException e) {
            LOGGER.error("getNotification encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("getNotification encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

    /**
     * Delete the single notification.
     *
     * @param notification
     *         the notification
     * @param request
     *         the request
     */
    @PostMapping(value = "deleteNotification")
    public Map <String, Object> delete (@RequestBody Notification notification, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = notificationService.delete(notification.getNotificationId(), request);
        } catch (HibernateException e) {
            LOGGER.error("deleteNotification encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("deleteNotification encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }


        return response;
    }


}
