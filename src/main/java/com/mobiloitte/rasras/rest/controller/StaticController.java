package com.mobiloitte.rasras.rest.controller;

import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class StaticController.
 */
@RestController
@RequestMapping("/rest")
class StaticController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(StaticController.class);

    /**
     * The static service.
     */
    @Autowired
    private StaticService staticService;

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;


    /**
     * User can contact to the admin when he/she provides some information from the mobile by filling the form.
     *
     * @param contactUs
     *         the contact us
     * @param request
     *         the request
     * @return the map
     */
    @PostMapping("contactUs")
    public Map <String, Object> contactUs (@RequestBody ContactUs contactUs, HttpServletRequest request) {
        Map <String, Object> response = new HashMap <>();
        try {
            response = staticService.save(contactUs, request);
        } catch (HibernateException e) {
            LOGGER.error("contactUs encountered an error - ", e);
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage("FAILED_TO_PERFORM_ACTION", null, request.getLocale()));
            response.put(Message.DATA, null);
        } catch (Exception e) {
            LOGGER.error("contactUs encountered an error - ", e);
            response.put(Message.CODE, Code.ERROR);
            response.put(Message.MESSAGE, messageSource.getMessage("ITERNAL_SERVER_ERROR", null, request.getLocale()));
            response.put(Message.DATA, null);
        }
        return response;
    }

}
