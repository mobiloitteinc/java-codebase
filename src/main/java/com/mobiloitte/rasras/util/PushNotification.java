package com.mobiloitte.rasras.util;


import com.mobiloitte.rasras.dao.NotificationDao;
import com.mobiloitte.rasras.dao.UserDao;
import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class PushNotification.
 */
@Component
public class PushNotification {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PushNotification.class);

    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    /**
     * The notification dao.
     */
    @Autowired
    private NotificationDao notificationDao;

    @Value("${fcmKey}")
    private String fcmKey;

    @Value("${fcmURL}")
    private String fcmUrl;

    /**
     * Send push notification.
     *
     * @param map
     *         the map
     * @throws Exception
     *         the exception
     */
    public void sendPushNotification (Map <String, Object> map) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("sendPushNotification needed more information -", map);
        Integer userId = (Integer) map.get("toUserId");

        if (userDao.get(userId).getNotificationStatus().equals(true)) {
            Map <String, Object> deviceTokens = getDeviceTokens(userId);
            ArrayList <String> androidDevices = (ArrayList <String>) deviceTokens.get("androidDevices");
            ArrayList <String> iosDevices = (ArrayList <String>) deviceTokens.get("iosDevices");
            if (!androidDevices.isEmpty()) {
                LOGGER.trace("sending notfication in androidDevices : " + androidDevices);
                map.put("androidDevices", androidDevices);
                sendAndroidNotification(map);
            }

            if (!iosDevices.isEmpty()) {
                LOGGER.info("sending notfication in appleDevices : " + iosDevices);
                for (String iosDevice : iosDevices) {
                    LOGGER.trace("sending notfication in apple device token: " + iosDevice);
                    sendIosNotification(iosDevice, map);
                }
            }
        }

    }

    /**
     * Gets the device tokens.
     *
     * @param userId
     *         the user id
     * @return the device tokens
     */
    private Map <String, Object> getDeviceTokens (Integer userId) {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("getDeviceTokens needed more information - userId :", userId);
        List <Device> deviceTokenList = notificationDao.getDeviceTokens(userId);
        List <String> androidDevice = new ArrayList <>();
        List <String> iosDevices = new ArrayList <>();
        Map <String, Object> response = new HashMap <>();


        if (deviceTokenList != null)
            for (Device device : deviceTokenList) {

                if (device.getDeviceType().equalsIgnoreCase("android") && !androidDevice.contains(device.getDeviceToken())) {
                    androidDevice.add(device.getDeviceToken());
                } else if (device.getDeviceType().equalsIgnoreCase("ios") && !iosDevices.contains(device.getDeviceToken())) {
                    iosDevices.add(device.getDeviceToken());
                }
            }

        response.put("androidDevices", androidDevice);
        response.put("iosDevices", iosDevices);

        return response;
    }


    /**
     * Send android notification.
     *
     * @param map
     *         the map
     * @throws JSONException
     */
    private void sendAndroidNotification (Map <String, Object> map) throws JSONException {
        if (LOGGER.isDebugEnabled())
            LOGGER.debug("debugging fcm send push notification function for android devices : " + map);

        try {

            String output;
            StringEntity input;

            StringBuilder responseData = new StringBuilder();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(fcmUrl);

            JSONObject info = new JSONObject();
            info.put("title", map.get("title")); // Notification title
            info.put("body", map.get("body")); // Notification body
            info.put("sound", "default");
            //info.put("icon", "myicon");
            info.put("vibrate", true);

            JSONObject json = new JSONObject();
            json.put("registration_ids", map.get("androidDevices"));
            json.put("delay_while_idle", false);
            json.put("time_to_live", 3);
            //	json.put("notification", info);

            JSONObject jsonData = new JSONObject();
            for (Map.Entry <String, Object> entry : map.entrySet())
                jsonData.put(entry.getKey(), entry.getValue());
            json.put("data", jsonData);
            json.put("priority", "high");

            input = new StringEntity(json.toString());
            input.setContentType("application/json");
            postRequest.setHeader("Authorization", "key=" + fcmKey);
            postRequest.setEntity(input);

            LOGGER.info("fcm push notification json structure: " + json.toString());
            CloseableHttpResponse response = httpClient.execute(postRequest);
            LOGGER.info("fcm push notification response :" + response);
            if (response.getStatusLine().getStatusCode() == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                while ((output = br.readLine()) != null) {
                    responseData.append(output);
                }

                httpClient.getConnectionManager().shutdown();
            } else {

                BufferedReader br = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
                while ((output = br.readLine()) != null) {
                    responseData.append(output);

                }

                httpClient.getConnectionManager().shutdown();
            }
        } catch (IOException | RuntimeException e) {
            LOGGER.error("exception in fcm send push notification function : " + map);
            LOGGER.error("exception is: : " + e);
        }


    }

    /**
     * Send ios notification.
     *
     * @param devicetoken
     *         the devicetoken
     * @param map
     *         the map
     */
    private void sendIosNotification (final String devicetoken, Map <String, Object> map) {
        if (LOGGER.isDebugEnabled())
            LOGGER.debug("debugging fcm send push notification function for apple devices : : " + map);
        BasicConfigurator.configure();
        try {

            final PushNotificationPayload payload = PushNotificationPayload.complex();
            Map <String, Object> shallowCopy = new HashMap <>();
            shallowCopy.putAll(map);


            JSONObject alert = new JSONObject();
            alert.put("title", map.get("title"));
            alert.put("body", map.get("body"));

            JSONObject aps = new JSONObject();
            aps.put("badge", 1);
            aps.put("alert", alert);
            aps.put("sound", "default");

            payload.addCustomDictionary("aps", aps);
            shallowCopy.remove("title");
            shallowCopy.remove("body");
            shallowCopy.remove("isGroupChat");
            payload.addCustomDictionary("custom", shallowCopy);

            Runnable runnable = new Runnable() {

                @Override
                public void run () {
                    try {
                        ClassLoader classLoader = getClass().getClassLoader();
                        File certificatePath = new File(classLoader.getResource("notfication/EnterpriseProfile.p12").getFile());  //Staging working...


                        String tempPath = certificatePath.getAbsolutePath().replaceAll("%20", " ");
                        File p12fileLocation = new File(tempPath);
                        if (!devicetoken.equalsIgnoreCase("deviceToken")) {
                            LOGGER.debug("payload started: " + map);
                            LOGGER.info("payload: " + payload.toString());
                            Push.payload(payload, p12fileLocation, "", true, devicetoken); // staging working...
                            //	Push.payload(payload, p12fileLocation, "", false, devicetoken); // For development
                            LOGGER.info("payload ended: " + map);

                        }
                    } catch (CommunicationException e) {
                        LOGGER.error("exception is: " + e);
                    } catch (KeystoreException e) {
                        LOGGER.error("exception is: " + e);
                    }
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();

        } catch (JSONException e) {
            LOGGER.error("exception is: " + e);
        } catch (Exception e) {
            LOGGER.error("exception is: " + e);
        }

    }


}
