package com.mobiloitte.rasras.util;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

// TODO: Auto-generated Javadoc

/**
 * The Class Email.
 */
@Component
public class Email {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Email.class);

    /**
     * The environment.
     */
    @Autowired
    private Environment environment;

    /**
     * Gets the email template.
     *
     * @param emailMap
     *         the email map
     * @return the email template
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    public String getEmailTemplate (Map <String, Object> emailMap) {
        if (LOGGER.isDebugEnabled())
            LOGGER.debug("debugging getEmailTemplate function : " + emailMap);
        VelocityEngine velocity = new VelocityEngine();

        Properties props = new Properties();
        ClassLoader classLoader = getClass().getClassLoader();
        File resourcePath = new File(classLoader.getResource("templates").getFile());
        props.put("file.resource.loader.path", resourcePath.getAbsolutePath());
        props.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
        velocity.init(props);

        Template emailTemplate = velocity.getTemplate(emailMap.get("templateName") + "");

        VelocityContext context = new VelocityContext();
        context.put("title", emailMap.get("title"));
        context.put("body", emailMap.get("body"));
        StringWriter writer = new StringWriter();
        emailTemplate.merge(context, writer);

        return writer.toString();
    }

    /**
     * Send.
     *
     * @param map
     *         the map
     * @return true, if successful
     */
    public void send (Map <String, Object> map) {
        if (LOGGER.isDebugEnabled())
            LOGGER.debug("debugging sendgrid send email function : " + map);
        String fromEmail = environment.getProperty("fromEmail");
        String sendGridKey = environment.getProperty("sendGridKey");

        SendGrid sendgrid = new SendGrid(sendGridKey);
        SendGrid.Email email = new SendGrid.Email();
        email.addTo(map.get("toEmail") + "");
        email.setFrom(fromEmail);
        email.setSubject(map.get("subject") + "");
        email.setHtml(map.get("body") + "");
        try {
            sendgrid.send(email);
            LOGGER.info("Email sent from " + fromEmail + " email address to " + map.get("toEmail") + " email address.");
        } catch (SendGridException e) {
            LOGGER.error("Exception in sending email from " + fromEmail + " email address to " + map.get("toEmail") + " email address.");
        }


    }


}
