package com.mobiloitte.rasras.util;

import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;


// TODO: Auto-generated Javadoc

/**
 * The Class StringGenerator.
 */
@Component
public class StringGenerator {


    /**
     * Generate random number.
     *
     * @param length
     *         the length
     * @return the string
     */
    public static String generateRandomNumber (int length) {

        String generatedOtp = new Date().getTime() + "";
        generatedOtp = generatedOtp.substring(generatedOtp.length() - length);
        return generatedOtp;
    }


    /**
     * Generate random string.
     *
     * @return the string
     */
    public static String generateRandomString () {
        SecureRandom random = new SecureRandom();
        return new BigInteger(30, random).toString(32);
    }

    /**
     * Random string.
     *
     * @param characterSet
     *         the character set
     * @param length
     *         the length
     * @return the string
     */
    public static String randomString (char[] characterSet, int length) {
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {

            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }


}
