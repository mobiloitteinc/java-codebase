package com.mobiloitte.rasras.util;

// TODO: Auto-generated Javadoc

/**
 * The Class Message.
 */
public class Message {

    /**
     * The Constant SUCCESS.
     */
    public static final String SUCCESS = "Success";

    /**
     * The Constant FAILURE.
     */
    public static final String FAILURE = "Failed to perform the action!!!";

    /**
     * The Constant ERROR.
     */
    public static final String ERROR = "Server encountered a problem!!!";

    /**
     * The Constant CODE.
     */
    public static final String CODE = "code";

    /**
     * The Constant MESSAGE.
     */
    public static final String MESSAGE = "message";

    /**
     * The Constant DATA.
     */
    public static final String DATA = "data";
}
