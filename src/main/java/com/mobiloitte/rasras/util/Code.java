package com.mobiloitte.rasras.util;

// TODO: Auto-generated Javadoc

/**
 * The Class Code.
 */
public class Code {

    /**
     * The Constant SUCCESS.
     */
    public static final Integer SUCCESS = 200;

    /**
     * The Constant FAILURE.
     */
    public static final Integer FAILURE = 201;

    /**
     * The Constant ERROR.
     */
    public static final Integer ERROR = 500;


    /**
     * The Constant BLOCKED.
     */
    public static final Integer BLOCKED = 401;
}




