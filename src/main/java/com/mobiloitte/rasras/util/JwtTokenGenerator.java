package com.mobiloitte.rasras.util;

import com.mobiloitte.rasras.security.JwtGenerator;
import com.mobiloitte.rasras.security.model.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


// TODO: Auto-generated Javadoc

/**
 * The Class JwtTokenGenerator.
 */
@Component
public class JwtTokenGenerator {

    /**
     * The jwt generator.
     */
    @Autowired
    private JwtGenerator jwtGenerator;


    /**
     * Generate.
     *
     * @param jwtUser
     *         the jwt user
     * @return the string
     */
    public String generate (JwtUser jwtUser) {

        return jwtGenerator.generate(jwtUser);

    }
}
