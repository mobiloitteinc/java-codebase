package com.mobiloitte.rasras.util;


import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Class Common.
 */
@Component
public class Common {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Common.class);


    /**
     * The cloudinary cloud name.
     */
    @Value("${cloudinaryCloudName}")
    private String cloudinaryCloudName;

    /**
     * The cloudinary api key.
     */
    @Value("${cloudinaryApiKey}")
    private String cloudinaryApiKey;

    /**
     * The cloudinary api secret key.
     */
    @Value("${cloudinaryApiSecretKey}")
    private String cloudinaryApiSecretKey;


    /**
     * Upload.
     *
     * @param imageData
     *         the image data
     * @return the string
     */
    public String upload (byte[] imageData) {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("upload image from byte needed more information");
        String url = null;


        try {

            Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                    "cloud_name", cloudinaryCloudName,
                    "api_key", cloudinaryApiKey,
                    "api_secret", cloudinaryApiSecretKey));
            Map <String, String> map = new HashMap <>();
            map.put("resource_type", "auto");
            Map result = cloudinary.uploader().upload(imageData, map);
            url = (String) result.get("secure_url");

        } catch (IOException e) {
            LOGGER.error("exception is: " + e);
        } catch (RuntimeException e) {
            LOGGER.error("exception is: " + e);
        }
        return url;
    }


    /**
     * Upload.
     *
     * @param base64Image
     *         the base 64 image
     * @return the string
     */
    public String upload (String base64Image) {
        if (LOGGER.isDebugEnabled())
            LOGGER.trace("upload image from base64 needed more information");
        String url = null;

        try {
            if (!StringUtils.isEmpty(base64Image)) {


                Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                        "cloud_name", cloudinaryCloudName,
                        "api_key", cloudinaryApiKey,
                        "api_secret", cloudinaryApiSecretKey));


                byte[] imageData = Base64.decodeBase64(base64Image);
                Map <String, String> map = new HashMap <>();
                map.put("resource_type", "auto");
                Map result = cloudinary.uploader().upload(imageData, map);
                LOGGER.info("result: " + result);
                url = (String) result.get("secure_url");

            }

        } catch (IOException e) {
            LOGGER.error("exception is: " + e);
        } catch (RuntimeException e) {
            LOGGER.error("exception is: " + e);
        }
        return url;
    }


}
