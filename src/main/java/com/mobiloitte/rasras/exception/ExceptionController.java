package com.mobiloitte.rasras.exception;

import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc

/**
 * The Class ExceptionController.
 */
@ControllerAdvice
class ExceptionController {

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Error H andler.
     *
     * @param ex
     *         the ex
     * @return the map
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map <String, Object> errorHAndler (MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List <FieldError> errorList = result.getFieldErrors();
        return processError(errorList);
    }

    /**
     * Process error.
     *
     * @param errors
     *         the errors
     * @return the map
     */
    private Map <String, Object> processError (List <FieldError> errors) {
        Map <String, Object> response = new LinkedHashMap <>();
        for (FieldError error : errors) {
            response.put(Message.CODE, Code.FAILURE);
            response.put(Message.MESSAGE, messageSource.getMessage(error, LocaleContextHolder.getLocale()));
            response.put(Message.DATA, null);

        }
        return response;
    }
}
