package com.mobiloitte.rasras.daoimpl;

import com.mobiloitte.rasras.dao.NotificationDao;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.User;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class NotificationDaoImpl.
 */
@Repository
@Transactional
public class NotificationDaoImpl implements NotificationDao {

    /**
     * The hibernate template.
     */
    @Autowired
    private HibernateTemplate hibernateTemplate;
    /**
     * The all activity size.
     */
    private int totalResults;
    /**
     * The current index.
     */
    private int currentIndex = 0;

    @Value("${pageDefaultSizeGlobal}")
    private String paginationDefaultSize;

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.NotificationDao#getNotifications(com.mobiloitte.rasras.entity.Notification)
     */
    public List <Object[]> getNotifications (Notification notificationManager) {
        Query query = hibernateTemplate.getSessionFactory().getCurrentSession()
                .createQuery(
                        "select n.notificationId,n.notificationTitle,n.notificationFrom,n.notificationTime,n.notificationDescription,n.notificationType,n.itemId,n.offerId from Notification n " +
                                " where n.notificationTo = :id and n.notificationDeleteStatus=:status order by n.notificationId DESC")
                .setParameter("id", notificationManager.getNotificationTo()).setParameter("status", false);

        totalResults = query.list().size();
        currentIndex = notificationManager.getPageNumber();
        query.setFirstResult(notificationManager.getPageNumber() * Integer.parseInt(paginationDefaultSize));
        query.setMaxResults(Integer.parseInt(paginationDefaultSize));

        return (List <Object[]>) query.list();
    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.NotificationDao#setNotificationStatus(com.mobiloitte.rasras.entity.User)
     */
    public int setNotificationStatus (User u1) {
        return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("update User u set u.notificationStatus=? where u.userId=?").setParameter(0, u1.getNotificationStatus()).setParameter(1, u1.getUserId()).executeUpdate();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.NotificationDao#getTotalResults()
     */
    public int getTotalResults () {
        return totalResults;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.NotificationDao#getCurrentIndex()
     */
    public int getCurrentIndex () {
        return currentIndex;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.NotificationDao#getNotification(int)
     */
    public String getNotification (int notificationId) {
        return (String) hibernateTemplate.getSessionFactory().getCurrentSession()
                .createQuery("select notificationDescription from Notification where notificationId = :notification_id").setParameter("notification_id", notificationId).uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.NotificationDao#getDeviceTokens(java.lang.Integer)
     */
    @Override
    public List <Device> getDeviceTokens (Integer userId) {

        return (List <Device>) hibernateTemplate.getSessionFactory().getCurrentSession()
                .createQuery("from Device where userId=:userId").setParameter("userId", userId).list();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.NotificationDao#get(java.lang.Integer)
     */
    @Override
    public Item get (Integer itemId) {
        return (Item) hibernateTemplate.getSessionFactory().getCurrentSession()
                .createQuery("from Item item where itemId=:itemId")
                .setParameter("itemId", itemId).uniqueResult();
    }

    @Override
    public void delete (Integer notificationId) {
        Notification notification = hibernateTemplate.getSessionFactory().getCurrentSession().load(Notification.class, notificationId);
        hibernateTemplate.getSessionFactory().getCurrentSession().delete(notification);
    }
}
