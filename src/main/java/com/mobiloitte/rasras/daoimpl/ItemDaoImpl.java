package com.mobiloitte.rasras.daoimpl;

import com.mobiloitte.rasras.dao.ItemDao;
import com.mobiloitte.rasras.entity.Comment;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.Like;
import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.Offer;
import com.mobiloitte.rasras.entity.Size;
import com.mobiloitte.rasras.entity.UserDetails;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;

// TODO: Auto-generated Javadoc

/**
 * The Class ItemDaoImpl.
 */
@Repository
@Transactional
public class ItemDaoImpl implements ItemDao {


    /**
     * The Constant MILES.
     */
    private static final String MILES = "MILES";
    /**
     * The Constant KMS.
     */
    private static final String KMS = "KMS";

    @Value("${pageDefaultSizeGlobal}")
    private String paginationDefaultSize;

    @Value("${itemSize}")
    private String itemSize;

    /**
     * The distance unit.
     */
    @Value("${distanceUnit}")
    private String distanceUnit;

    /**
     * The specific distance.
     */
    @Value("${searchDistanceCriteria}")
    private String searchDistanceCriteria;

    /**
     * The hibernate template.
     */
    @Autowired
    private HibernateTemplate hibernateTemplate;


    /**
     * The all activity size.
     */
    private int allActivitySize;

    /**
     * The current index.
     */
    private int currentIndex = 0;


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#save(com.mobiloitte.rasras.entity.Item)
     */
    @Override
    public int save (Item item) {

        return (int) hibernateTemplate.getSessionFactory().getCurrentSession().save(item);
    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getSize()
     */
    @SuppressWarnings("unchecked")
    public List <Size> getSize () {
        return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Size").list();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getColor()
     */
    @SuppressWarnings("unchecked")
    public List <Color> getColor () {
        return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Color").list();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getCategory()
     */
    @SuppressWarnings("unchecked")
    public List <Category> getCategory () {
        return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Category").list();
    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getItem(int)
     */
    public List <Item> getItem (int categoryId) {
        List <Item> itemList = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Item where deleteStatus = 0 and  item_category_id=:itemCategoryId")

                .setParameter("itemCategoryId", categoryId).list();
        for (Item item : itemList) {
            Hibernate.initialize(item.getItemImages());
            Hibernate.initialize(item.getItemComments());
        }

        return itemList;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#addLike(com.mobiloitte.rasras.entity.Like)
     */
    public void addLike (Like like) {

        hibernateTemplate.getSessionFactory().getCurrentSession().save(like);
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#updateLike(com.mobiloitte.rasras.entity.Like)
     */
    public int updateLike (Like activityLikeDislike) {
        if (activityLikeDislike.getLike_t().equals(1))
            saveNotificationOnLike(activityLikeDislike);
        Query qry = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("update Like likes set likes.like_t=? where likes.itemLikeId=? and likes.likedBy=?");
        qry.setParameter(0, activityLikeDislike.getLike_t());
        qry.setParameter(1, activityLikeDislike.getItemLikeId());
        qry.setParameter(2, activityLikeDislike.getLikedBy());
        return qry.executeUpdate();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#addLikeCount(int)
     */
    public void addLikeCount (int itemId) {
        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("update Item item set item.itemLikeCount=item.itemLikeCount+1 where item.itemId=?");
        query.setParameter(0, itemId);
        query.executeUpdate();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#addCommentCount(int)
     */
    public void addCommentCount (int itemId) {
        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("update Item item set item.itemCommentCount=item.itemCommentCount+1 where item.itemId=?");
        query.setParameter(0, itemId);
        query.executeUpdate();
    }

    /**
     * Gets the item comment count.
     *
     * @param itemId
     *         the item id
     * @return the item comment count
     */
    public int getItemCommentCount (int itemId) {
        return (int) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("select item.itemCommentCount from Item item where item.itemId=:itemId").setParameter("itemId", itemId).uniqueResult();
    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#saveNotificationOnLike(com.mobiloitte.rasras.entity.Like)
     */
    public Notification saveNotificationOnLike (Like like) {
        Item item = hibernateTemplate.getSessionFactory().getCurrentSession().get(Item.class, like.getItemLikeId());
        UserDetails userDetails = hibernateTemplate.getSessionFactory().getCurrentSession().get(UserDetails.class, like.getLikedBy());
        Notification notificationManager = new Notification();
        notificationManager.setNotificationType("like");
        notificationManager.setItemId(like.getItemLikeId());
        if (!Objects.equals(like.getLikedBy(), item.getUserItemId())) {

            notificationManager.setNotificationTitle("Favourite Notification.");
            if (userDetails.getLastName() != null)
                notificationManager.setNotificationDescription(userDetails.getFirstName() + " " + userDetails.getLastName() + " has added your item in his favourite list.");
            else
                notificationManager.setNotificationDescription(userDetails.getFirstName() + " has added your item in his favourite list.");

            notificationManager.setNotificationFrom(like.getLikedBy());
            notificationManager.setNotificationTo(item.getUserItemId());
            hibernateTemplate.getSessionFactory().getCurrentSession().save(notificationManager);
        } else {
            return null;
        }
        return notificationManager;
    }


    /**
     * Gets the item like count.
     *
     * @param itemId
     *         the item id
     * @return the item like count
     */
    public int getItemLikeCount (int itemId) {
        return (int) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("select item.itemLikeCount from Item item where item.itemId=:itemId").setParameter("itemId", itemId).uniqueResult();

    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#subtractLikeCount(int)
     */
    public void subtractLikeCount (int itemId) {
        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("update Item item set item.itemLikeCount=item.itemLikeCount-1 where item.itemId=?");
        query.setParameter(0, itemId);
        query.executeUpdate();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#addComment(com.mobiloitte.rasras.entity.Comment)
     */
    public int addComment (Comment comment) {

        return (int) hibernateTemplate.getSessionFactory().getCurrentSession().save(comment);
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#saveNotificationOnComment(com.mobiloitte.rasras.entity.Comment)
     */
    public Notification saveNotificationOnComment (Comment comment) {
        Item item = hibernateTemplate.getSessionFactory().getCurrentSession().get(Item.class, comment.getItemCommentId());
        UserDetails userDetails = hibernateTemplate.getSessionFactory().getCurrentSession().get(UserDetails.class, comment.getCommentBy());
        Notification notificationManager = new Notification();
        notificationManager.setNotificationType("comment");
        notificationManager.setItemId(comment.getItemCommentId());
        if (!Objects.equals(comment.getCommentBy(), item.getUserItemId())) {

            notificationManager.setNotificationTitle("Comment notification.");
            if (userDetails.getLastName() != null)
                notificationManager.setNotificationDescription(userDetails.getFirstName() + " " + userDetails.getLastName() + " has commented on your item.");
            else
                notificationManager.setNotificationDescription(userDetails.getFirstName() + " has commented on your item.");

            notificationManager.setNotificationFrom(comment.getCommentBy());
            notificationManager.setNotificationTo(item.getUserItemId());
            hibernateTemplate.getSessionFactory().getCurrentSession().save(notificationManager);
        } else {
            return null;
        }
        return notificationManager;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#unlike(int, int)
     */
    @Override
    public void unlike (int userId, int itemId) {


        hibernateTemplate.getSessionFactory().getCurrentSession()
                .createQuery("delete from Like where itemLikeId=:itemLikeId and likedBy=:likedBy")
                .setParameter("itemLikeId", itemId)
                .setParameter("likedBy", userId).executeUpdate();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#isOffered(java.lang.Integer, java.lang.Integer, java.lang.Integer)
     */
    @Override
    public Offer isOffered (Integer offerBy, Integer itemId, Integer offerTo) {
        return (Offer) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Offer offer where offerBy=:offerBy and" +
                " offerOnItemId=:offerOnItemId and  offerTo=:offerTo and deleteStatus=:deleteStatus")
                .setParameter("offerBy", offerBy)
                .setParameter("offerOnItemId", itemId)
                .setParameter("offerTo", offerTo)
                .setParameter("deleteStatus", 0).uniqueResult();
    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getItemDetail(com.mobiloitte.rasras.entity.Item)
     */
    public Map getItemDetail (Item item) {
        int unitValue = 0;
        if (MILES.equalsIgnoreCase(distanceUnit)) {
            unitValue = 3959;
        } else if (KMS.equalsIgnoreCase(distanceUnit)) {
            unitValue = 6371;
        }


        String hqlQuery = "select new map(item.itemId as itemId,item.itemTitle as itemTitle, item.colorId as  colorId," +
                " item.contactNumber as contactNumber, item.itemCategoryId as itemCategoryId, item.itemCommentCount as itemCommentCount, " +
                " item.itemCreationTime as itemCreationTime, item.itemDescription as itemDescription," +
                " item.itemLikeCount as itemLikeCount, item.itemTags as itemTags, item.itemTitle as itemTitle," +
                " item.latitude as latitude, item.longitude as longitude, item.material as material, item.pickUpAddress as pickUpAddress," +
                " item.sizeId as sizeId, item.userItemId as userItemId, item.yearOfPurchase as yearOfPurchase,round(" +
                +Integer.parseInt(String.valueOf(unitValue)) + "* acos (cos ( radians( " + item.getLatitude() + ") )* cos( radians( latitude ) ) * " +
                " cos( radians( longitude ) - radians( " + item.getLongitude() + ") )+ sin ( radians(" + item.getLatitude() + ") ) * sin( radians( latitude ) )),2) as distance)" +
                " from Item as item where itemId=" + item.getItemId();
        return (Map) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hqlQuery).uniqueResult();

    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getComments(int, int)
     */
    public List getComments (int itemId, int pageNumber) {
        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Comment comment" +
                " where comment.itemCommentId=:itemCommentId order by commentId desc").setParameter("itemCommentId", itemId);

        allActivitySize = query.list().size();
        currentIndex = pageNumber;
        query.setFirstResult(pageNumber * Integer.parseInt(paginationDefaultSize));
        query.setMaxResults(Integer.parseInt(paginationDefaultSize));

        return query.list();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getHomeItems(com.mobiloitte.rasras.entity.Item)
     */
    @Override
    public List getHomeItems (Item item) {
        int unitValue = 0;
        if (MILES.equalsIgnoreCase(distanceUnit)) {
            unitValue = 3959;
        } else if (KMS.equalsIgnoreCase(distanceUnit)) {
            unitValue = 6371;
        }

        String hqlQuery = "select new map(item.itemId as itemId,item.itemTitle as itemTitle, item.colorId as  colorId," +
                " item.contactNumber as contactNumber, item.itemCategoryId as itemCategoryId, item.itemCommentCount as itemCommentCount, " +
                " item.itemCreationTime as itemCreationTime, item.itemDescription as itemDescription," +
                " item.itemLikeCount as itemLikeCount, item.itemTags as itemTags, item.itemTitle as itemTitle," +
                " item.latitude as latitude, item.longitude as longitude, item.material as material, item.pickUpAddress as pickUpAddress," +
                " item.sizeId as sizeId, item.userItemId as userItemId, item.yearOfPurchase as yearOfPurchase," +
                " round(" + Integer.parseInt(String.valueOf(unitValue)) +
                " * acos (cos ( radians( " + item.getLatitude() + ") )* cos( radians( latitude ) ) * " +
                " cos( radians( longitude ) - radians( " + item.getLongitude() + ") )+ sin ( radians(" + item.getLatitude() + ") )" +
                " * sin( radians( latitude ) )),2) as distance,item.deleteStatus as deleteStatus)" +
                " from Item as item where deleteStatus=" + 0 + " and itemCategoryId=" + item.getItemCategoryId() + " group by itemId having col_18_0_ <" + searchDistanceCriteria + " order by itemId DESC";

        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hqlQuery);

        allActivitySize = query.list().size();
        currentIndex = item.getPageNumber();
        query.setFirstResult(item.getPageNumber() * Integer.parseInt(itemSize));
        query.setMaxResults(Integer.parseInt(itemSize));


        return query.list();

    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getItemImages(int)
     */
    public List getItemImages (int itemId) {
        return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Image m where m.itemImageId=:itemImageId").setParameter("itemImageId", itemId).list();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getFavouriteItems(java.lang.Integer, int)
     */
    @Override
    public List getFavouriteItems (Item item) {

        int unitValue = 0;
        if (MILES.equalsIgnoreCase(distanceUnit)) {
            unitValue = 3959;
        } else if (KMS.equalsIgnoreCase(distanceUnit)) {
            unitValue = 6371;
        }

        String hqlQuery = "select new map(item.itemId as itemId,item.itemTitle as itemTitle, item.colorId as  colorId," +
                " item.contactNumber as contactNumber, item.itemCategoryId as itemCategoryId, item.itemCommentCount as itemCommentCount, " +
                " item.itemCreationTime as itemCreationTime, item.itemDescription as itemDescription," +
                " item.itemLikeCount as itemLikeCount, item.itemTags as itemTags, item.itemTitle as itemTitle," +
                " item.latitude as latitude, item.longitude as longitude, item.material as material, item.pickUpAddress as pickUpAddress," +
                " item.sizeId as sizeId, item.userItemId as userItemId, item.yearOfPurchase as yearOfPurchase,round(" +
                +Integer.parseInt(String.valueOf(unitValue)) + "* acos (cos ( radians( " + item.getLatitude() + ") " +
                " )* cos( radians( latitude ) ) * " +
                " cos( radians( longitude ) - radians( " + item.getLongitude() + ") )+" +
                " sin ( radians(" + item.getLatitude() + ") ) * sin( radians( latitude ) )),2) as distance,item.deleteStatus as deleteStatus)" +
                " from Item as item where  deleteStatus=" + 0 + " and  item.itemId in ( select likes.itemLikeId from Like as likes where likes.likedBy = " + item.getUserItemId() + ") group by itemId  order by itemId DESC";


        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hqlQuery);

        allActivitySize = query.list().size();
        currentIndex = item.getPageNumber();
        query.setFirstResult(item.getPageNumber() * Integer.parseInt(itemSize));
        query.setMaxResults(Integer.parseInt(itemSize));
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#search(java.lang.String, com.mobiloitte.rasras.entity.Item)
     */
    @Override
    public List search (String itemName, Item item) {

        int unitValue = 0;
        if (MILES.equalsIgnoreCase(distanceUnit)) {
            unitValue = 3959;
        } else if (KMS.equalsIgnoreCase(distanceUnit)) {
            unitValue = 6371;
        }

        String hqlQuery = "select new map(item.itemId as itemId,item.itemTitle as itemTitle, item.colorId as  colorId," +
                " item.contactNumber as contactNumber, item.itemCategoryId as itemCategoryId, item.itemCommentCount as itemCommentCount, " +
                " item.itemCreationTime as itemCreationTime, item.itemDescription as itemDescription," +
                " item.itemLikeCount as itemLikeCount, item.itemTags as itemTags, item.itemTitle as itemTitle," +
                " item.latitude as latitude, item.longitude as longitude, item.material as material, item.pickUpAddress as pickUpAddress," +
                " item.sizeId as sizeId, item.userItemId as userItemId, item.yearOfPurchase as yearOfPurchase,round(" +
                +Integer.parseInt(String.valueOf(unitValue)) + "* acos (cos ( radians( " + item.getLatitude() + ") " +
                " )* cos( radians( item.latitude ) ) * " +
                " cos( radians( item.longitude ) - radians( " + item.getLongitude() + ") )+" +
                " sin ( radians(" + item.getLatitude() + ") ) * sin( radians( latitude ) )),2) as distance,item.deleteStatus as deleteStatus)" +
                " from Item as item where  item.deleteStatus=" + 0 + " and  item.itemTitle LIKE '%" + itemName + "%' group by item.itemId having  col_18_0_ <" + searchDistanceCriteria + " order by itemId DESC";


        Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hqlQuery);


        if (query.list().isEmpty()) {


            String hqlQuery1 = "select new map(item.itemId as itemId,item.itemTitle as itemTitle, item.colorId as  colorId," +
                    " item.contactNumber as contactNumber, item.itemCategoryId as itemCategoryId, item.itemCommentCount as itemCommentCount, " +
                    " item.itemCreationTime as itemCreationTime, item.itemDescription as itemDescription," +
                    " item.itemLikeCount as itemLikeCount, item.itemTags as itemTags, item.itemTitle as itemTitle," +
                    " item.latitude as latitude, item.longitude as longitude, item.material as material, item.pickUpAddress as pickUpAddress," +
                    " item.sizeId as sizeId, item.userItemId as userItemId, item.yearOfPurchase as yearOfPurchase,round(" +
                    +Integer.parseInt(String.valueOf(unitValue)) + "* acos (cos ( radians( " + item.getLatitude() + ") " +
                    " )* cos( radians( item.latitude ) ) * " +
                    " cos( radians( item.longitude ) - radians( " + item.getLongitude() + ") )+" +
                    " sin ( radians(" + item.getLatitude() + ") ) * sin( radians( item.latitude ) )),2) as distance,item.deleteStatus as deleteStatus)" +
                    " from User as user left join user.items as item left join user.userDetails as ud" +
                    "  where  item.deleteStatus=" + 0 + " and concat(ud.firstName,' ',ud.lastName)  LIKE '%" + itemName + "%' group by item.itemId having  col_18_0_ <" + searchDistanceCriteria + " order by itemId DESC";


            query = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hqlQuery1);


        }


        allActivitySize = query.list().size();
        currentIndex = item.getPageNumber();
        query.setFirstResult(item.getPageNumber() * Integer.parseInt(itemSize));
        query.setMaxResults(Integer.parseInt(itemSize));

        return query.list();

    }


    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#isLiked(int, int)
     */
    public Like isLiked (int itemId, int userId) {

        String hql = "select likes from Like likes where likes.itemLikeId=:itemLikeId and likes.likedBy=:userId";

        return (Like) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery(hql)
                .setParameter("itemLikeId", itemId).setParameter("userId", userId).uniqueResult();

    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getAllActivitySize()
     */
    public int getAllActivitySize () {
        return allActivitySize;
    }

    /**
     * Sets the all activity size.
     *
     * @param allActivitySize
     *         the new all activity size
     */
    public void setAllActivitySize (int allActivitySize) {
        this.allActivitySize = allActivitySize;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getCurrentIndex()
     */
    public int getCurrentIndex () {
        return currentIndex;
    }

    /**
     * Sets the current index.
     *
     * @param currentIndex
     *         the new current index
     */
    public void setCurrentIndex (int currentIndex) {
        this.currentIndex = currentIndex;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getUserItems(int)
     */
    @Override
    public List <Item> getUserItems (int userId) {
        return (List <Item>) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Item where userItemId =:userItemId").setParameter("userItemId", userId).list();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getColor(int)
     */
    public Color getColor (int colorId) {
        return hibernateTemplate.getSessionFactory().getCurrentSession().get(Color.class, colorId);
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.ItemDao#getSize(int)
     */
    public Size getSize (int sizeId) {
        return hibernateTemplate.getSessionFactory().getCurrentSession().get(Size.class, sizeId);
    }

    public int getCategoryIdFromItem (int itemId) {
        return (int) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("select itemCategoryId from Item where itemId=:itemId").setParameter("itemId", itemId).uniqueResult();
    }

    public Category getCategory (int categoryId) {
        return hibernateTemplate.getSessionFactory().getCurrentSession().get(Category.class, categoryId);
    }


}