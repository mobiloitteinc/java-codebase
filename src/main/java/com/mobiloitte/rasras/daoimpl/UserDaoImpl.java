package com.mobiloitte.rasras.daoimpl;

import com.mobiloitte.rasras.dao.UserDao;
import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.model.ChangePassword;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


// TODO: Auto-generated Javadoc

/**
 * The Class UserDaoImpl.
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    /**
     * The hibernate template.
     */
    @Autowired
    private HibernateTemplate hibernateTemplate;

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.UserDao#get(java.lang.Integer)
     */
    @Override
    public User get (Integer userId) {

        User user1 = hibernateTemplate.getSessionFactory().getCurrentSession().load(User.class, userId);
        Hibernate.initialize(user1.getUserDetails());
        return user1;
    }

    /**
     * Gets the user by id.
     *
     * @param user
     *         the user
     * @return the user by id
     * @throws Exception
     *         the exception
     */
    public User getUserById (User user) {
        User u1 = hibernateTemplate.getSessionFactory().getCurrentSession().load(User.class, user.getUserId());
        Hibernate.initialize(u1.getUserDetails());
        return u1;
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.UserDao#get(int)
     */
    public UserDetails get (int id) {
        return hibernateTemplate.getSessionFactory().getCurrentSession().get(UserDetails.class, id);
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.UserDao#change(com.mobiloitte.rasras.model.ChangePassword)
     */
    @Override
    public int change (ChangePassword changePassword) {
        Query changePasswordQuery = hibernateTemplate.getSessionFactory().getCurrentSession()
                .createQuery("update User set password =:newPassword where userId =:userId")
                .setParameter("newPassword", changePassword.getNewPassword())
                .setParameter("userId", changePassword.getUserId());
        return changePasswordQuery.executeUpdate();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.UserDao#update(com.mobiloitte.rasras.entity.UserDetails)
     */
    @Override
    public UserDetails update (UserDetails userDetails) {
        return (UserDetails) hibernateTemplate.getSessionFactory().getCurrentSession().merge(userDetails);
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.UserDao#verify(com.mobiloitte.rasras.entity.OTP)
     */
    @Override
    public String verify (OTP otp) {
        return (String) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("select otp from OTP where userId = :userId and otp =:otp").setParameter("userId", otp.getUserId()).setParameter("otp", otp.getOtp()).uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.mobiloitte.rasras.dao.UserDao#getUserWithItem(java.lang.Integer)
     */
    public User getUserWithItem (Integer userId) {

        User user1 = hibernateTemplate.getSessionFactory().getCurrentSession().load(User.class, userId);
        Hibernate.initialize(user1.getUserDetails());
        Hibernate.initialize(user1.getItems());
        return user1;
    }

}
