package com.mobiloitte.rasras.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;
import java.util.List;
import java.util.Properties;

// TODO: Auto-generated Javadoc

/**
 * The Class RepositoryConfig.
 */
@EnableWebMvc
@Configuration
class RepositoryConfig extends WebMvcConfigurerAdapter {

    /**
     * The driver class name.
     */
    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    /**
     * The url.
     */
    @Value("${spring.datasource.url}")
    private String url;

    /**
     * The username.
     */
    @Value("${spring.datasource.username}")
    private String username;

    /**
     * The password.
     */
    @Value("${spring.datasource.password}")
    private String password;

    /**
     * The hibernate dialect.
     */
    @Value("${spring.jpa.properties.hibernate.dialect}")
    private String hibernateDialect;

    /**
     * The hibernate show sql.
     */
    @Value("${spring.jpa.show-sql}")
    private String hibernateShowSql;

    /**
     * The hibernate hbm 2 ddl auto.
     */
    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String hibernateHbm2ddlAuto;


    /**
     * Jackson message converter.
     *
     * @return the mapping jackson 2 http message converter
     */
    private MappingJackson2HttpMessageConverter jacksonMessageConverter () {
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.getSerializerProvider().setNullKeySerializer(new MyNullKeySerializer());
        //Registering Hibernate4Module to support lazy objects
        mapper.registerModule(new Hibernate4Module().disable(Hibernate4Module.Feature.USE_TRANSIENT_ANNOTATION));

        messageConverter.setObjectMapper(mapper);
        return messageConverter;

    }

    /**
     * Configure message converters.
     *
     * @param converters
     *         the converters
     */
    @Override
    public void configureMessageConverters (List <HttpMessageConverter <?>> converters) {
        //Here we add our custom-configured HttpMessageConverter
        converters.add(jacksonMessageConverter());
        super.configureMessageConverters(converters);
    }


    /**
     * Gets the data source.
     *
     * @return the data source
     */
    @Bean()
    public DataSource getDataSource () {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        return ds;
    }

    /**
     * Transaction manager.
     *
     * @param sessionFactory
     *         the session factory
     * @return the hibernate transaction manager
     */
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager (SessionFactory sessionFactory) {
        HibernateTransactionManager htm = new HibernateTransactionManager();
        htm.setSessionFactory(sessionFactory);
        return htm;
    }

    /**
     * Gets the session factory.
     *
     * @return the session factory
     */
    @Bean
    public LocalSessionFactoryBean getSessionFactory () {
        LocalSessionFactoryBean asfb = new LocalSessionFactoryBean();
        asfb.setDataSource(getDataSource());
        asfb.setHibernateProperties(getHibernateProperties());
        asfb.setPackagesToScan("com.mobiloitte.rasras.entity");
        return asfb;
    }

    /**
     * Gets the hibernate template.
     *
     * @param sessionFactory
     *         the session factory
     * @return the hibernate template
     */
    @Bean
    @Autowired
    public HibernateTemplate getHibernateTemplate (SessionFactory sessionFactory) {
        return new HibernateTemplate(sessionFactory);
    }


    /**
     * Gets the hibernate properties.
     *
     * @return the hibernate properties
     */
    @Bean
    public Properties getHibernateProperties () {
        Properties properties = new Properties();
        properties.put("spring.jpa.properties.hibernate.dialect", hibernateDialect);
        properties.put("spring.jpa.show-sql", hibernateShowSql);
        properties.put("spring.jpa.hibernate.ddl-auto", hibernateHbm2ddlAuto);

        properties.put("hibernate.connection.characterEncoding", "utf-8");
        properties.put("hibernate.connection.CharSet", "utf-8");
        properties.put("hibernate.connection.useUnicode", true);

        properties.put("jackson.serialization.INDENT_OUTPUT", true);
        properties.put("http.encoding.charset", "UTF-8");
        properties.put("http.encoding.enabled", true);
        properties.put("http.encoding.force", true);

        return properties;
    }


    /**
     * Multipart resolver.
     *
     * @return the multipart resolver
     */
    @Bean
    public MultipartResolver multipartResolver () {
        return new StandardServletMultipartResolver();
    }


    /**
     * Multipart config element.
     *
     * @return the multipart config element
     */
    @Bean
    public MultipartConfigElement multipartConfigElement () {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("10MB");
        factory.setMaxRequestSize("10MB");
        return factory.createMultipartConfig();
    }
}