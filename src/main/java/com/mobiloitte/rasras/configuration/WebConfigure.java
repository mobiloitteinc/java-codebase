package com.mobiloitte.rasras.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

// TODO: Auto-generated Javadoc

/**
 * The Class WebConfigure.
 */
@Configuration
class WebConfigure extends WebMvcConfigurationSupport {

    /**
     * The Constant CLASSPATH_RESOURCE_LOCATIONS.
     */
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
            "classpath:/META-INF/resources/", "classpath:/resources/**",
            "classpath:/static/", "classpath:/public/"};

    /**
     * Adds the resource handlers.
     *
     * @param registry
     *         the registry
     */
    @Override
    public void addResourceHandlers (ResourceHandlerRegistry registry) {
        if (!registry.hasMappingForPattern("/webjars/**")) {
            registry.addResourceHandler("/webjars/**").addResourceLocations(
                    "classpath:/META-INF/resources/**");
        }
        if (!registry.hasMappingForPattern("/**")) {
            registry.addResourceHandler("/**").addResourceLocations(
                    CLASSPATH_RESOURCE_LOCATIONS);
        }
    }
}
