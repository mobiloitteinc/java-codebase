package com.mobiloitte.rasras.configuration;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;


// TODO: Auto-generated Javadoc

/**
 * The Class AppConfig.
 */
@Configuration
class AppConfig {

    /**
     * Gets the property placeholder configurer.
     *
     * @return the property placeholder configurer
     */
    @Bean
    public static PropertyPlaceholderConfigurer getPropertyPlaceholderConfigurer () {

        PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
        ppc.setLocation(new ClassPathResource("application.properties"));
        ppc.setIgnoreUnresolvablePlaceholders(true);
        return ppc;
    }


}