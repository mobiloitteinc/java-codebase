package com.mobiloitte.rasras.configuration;


import com.mobiloitte.rasras.security.JwtAuthenticationEntryPoint;
import com.mobiloitte.rasras.security.JwtAuthenticationProvider;
import com.mobiloitte.rasras.security.JwtAuthenticationTokenFilter;
import com.mobiloitte.rasras.security.JwtSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Collections;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtSecurityConfig.
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
class JwtSecurityConfig extends WebSecurityConfigurerAdapter {


    /**
     * The authentication provider.
     */
    @Autowired
    private JwtAuthenticationProvider authenticationProvider;

    /**
     * The entry point.
     */
    @Autowired
    private JwtAuthenticationEntryPoint entryPoint;

    /**
     * Authentication manager.
     *
     * @return the authentication manager
     */
    @Bean
    public AuthenticationManager authenticationManager () {
        return new ProviderManager(Collections.singletonList(authenticationProvider));
    }

    /**
     * Authentication token filter.
     *
     * @return the jwt authentication token filter
     */
    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilter () {
        JwtAuthenticationTokenFilter filter = new JwtAuthenticationTokenFilter();
        filter.setAuthenticationManager(authenticationManager());
        filter.setAuthenticationSuccessHandler(new JwtSuccessHandler());
        return filter;
    }


    /**
     * Configure.
     *
     * @param http
     *         the http
     * @throws Exception
     *         the exception
     */
    @Override
    protected void configure (HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests().antMatchers("**/rest/**").authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(entryPoint)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(authenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        http.headers().cacheControl();

    }
}