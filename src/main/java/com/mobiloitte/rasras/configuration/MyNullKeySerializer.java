package com.mobiloitte.rasras.configuration;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

// TODO: Auto-generated Javadoc

/**
 * The Class MyNullKeySerializer.
 */
class MyNullKeySerializer extends JsonSerializer <Object> {

    /**
     * Serialize.
     *
     * @param nullKey
     *         the null key
     * @param jsonGenerator
     *         the json generator
     * @param unused
     *         the unused
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    @Override
    public void serialize (Object nullKey, JsonGenerator jsonGenerator, SerializerProvider unused)
            throws IOException {
        jsonGenerator.writeFieldName("");
    }
}
