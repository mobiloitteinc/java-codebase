package com.mobiloitte.rasras.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtSuccessHandler.
 */
public class JwtSuccessHandler implements AuthenticationSuccessHandler {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtSuccessHandler.class);

    /**
     * On authentication success.
     *
     * @param httpServletRequest
     *         the http servlet request
     * @param httpServletResponse
     *         the http servlet response
     * @param authentication
     *         the authentication
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     * @throws ServletException
     *         the servlet exception
     */
    @Override
    public void onAuthenticationSuccess (HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        LOGGER.info("Successfully Authentication");

    }
}
