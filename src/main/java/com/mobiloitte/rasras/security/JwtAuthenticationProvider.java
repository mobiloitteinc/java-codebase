package com.mobiloitte.rasras.security;


import com.mobiloitte.rasras.security.model.JwtAuthenticationToken;
import com.mobiloitte.rasras.security.model.JwtUser;
import com.mobiloitte.rasras.security.model.JwtUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtAuthenticationProvider.
 */
@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    /**
     * The validator.
     */
    @Autowired
    private JwtValidator validator;

    /**
     * Additional authentication checks.
     *
     * @param userDetails
     *         the user details
     * @param usernamePasswordAuthenticationToken
     *         the username password authentication token
     * @throws AuthenticationException
     *         the authentication exception
     */
    @Override
    protected void additionalAuthenticationChecks (UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

    }

    /**
     * Retrieve user.
     *
     * @param username
     *         the username
     * @param usernamePasswordAuthenticationToken
     *         the username password authentication token
     * @return the user details
     * @throws AuthenticationException
     *         the authentication exception
     */
    @Override
    protected UserDetails retrieveUser (String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) usernamePasswordAuthenticationToken;
        String token = jwtAuthenticationToken.getToken();

        JwtUser jwtUser = validator.validate(token);

        if (jwtUser == null) {
            throw new RuntimeException("JWT Token is incorrect");
        }

        List <GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList(jwtUser.getRole());
        return new JwtUserDetails(jwtUser.getUserName(), jwtUser.getId(),
                token,
                grantedAuthorities);
    }

    /**
     * Supports.
     *
     * @param aClass
     *         the a class
     * @return true, if successful
     */
    @Override
    public boolean supports (Class <?> aClass) {
        return JwtAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
