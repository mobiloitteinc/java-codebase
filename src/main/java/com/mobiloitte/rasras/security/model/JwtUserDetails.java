package com.mobiloitte.rasras.security.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtUserDetails.
 */
public class JwtUserDetails implements UserDetails {

    /**
     * The user name.
     */
    private final String userName;

    /**
     * The token.
     */
    private final String token;

    /**
     * The id.
     */
    private final Long id;

    /**
     * The authorities.
     */
    private final Collection <? extends GrantedAuthority> authorities;


    /**
     * Instantiates a new jwt user details.
     *
     * @param userName
     *         the user name
     * @param id
     *         the id
     * @param token
     *         the token
     * @param grantedAuthorities
     *         the granted authorities
     */
    public JwtUserDetails (String userName, long id, String token, List <GrantedAuthority> grantedAuthorities) {

        this.userName = userName;
        this.id = id;
        this.token = token;
        this.authorities = grantedAuthorities;
    }

    /**
     * Gets the authorities.
     *
     * @return the authorities
     */
    @Override
    public Collection <? extends GrantedAuthority> getAuthorities () {
        return authorities;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    @Override
    public String getPassword () {
        return null;
    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    @Override
    public String getUsername () {
        return userName;
    }

    /**
     * Checks if is account non expired.
     *
     * @return true, if is account non expired
     */
    @Override
    public boolean isAccountNonExpired () {
        return true;
    }

    /**
     * Checks if is account non locked.
     *
     * @return true, if is account non locked
     */
    @Override
    public boolean isAccountNonLocked () {
        return true;
    }

    /**
     * Checks if is credentials non expired.
     *
     * @return true, if is credentials non expired
     */
    @Override
    public boolean isCredentialsNonExpired () {
        return true;
    }

    /**
     * Checks if is enabled.
     *
     * @return true, if is enabled
     */
    @Override
    public boolean isEnabled () {
        return true;
    }


    /**
     * Gets the user name.
     *
     * @return the user name
     */
    public String getUserName () {
        return userName;
    }

    /**
     * Gets the token.
     *
     * @return the token
     */
    public String getToken () {
        return token;
    }


    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId () {
        return id;
    }

}
