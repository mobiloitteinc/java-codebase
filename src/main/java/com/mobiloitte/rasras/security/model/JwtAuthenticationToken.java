package com.mobiloitte.rasras.security.model;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtAuthenticationToken.
 */
public class JwtAuthenticationToken extends UsernamePasswordAuthenticationToken {

    /**
     * The token.
     */
    private String token;

    /**
     * Instantiates a new jwt authentication token.
     *
     * @param token
     *         the token
     */
    public JwtAuthenticationToken (String token) {
        super(null, null);
        this.token = token;
    }

    /**
     * Gets the token.
     *
     * @return the token
     */
    public String getToken () {
        return token;
    }

    /**
     * Sets the token.
     *
     * @param token
     *         the new token
     */
    public void setToken (String token) {
        this.token = token;
    }

    /**
     * Gets the credentials.
     *
     * @return the credentials
     */
    @Override
    public Object getCredentials () {
        return null;
    }

    /**
     * Gets the principal.
     *
     * @return the principal
     */
    @Override
    public Object getPrincipal () {
        return null;
    }
}
