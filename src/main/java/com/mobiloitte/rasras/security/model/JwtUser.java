package com.mobiloitte.rasras.security.model;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtUser.
 */
public class JwtUser {

    /**
     * The user name.
     */
    private String userName;

    /**
     * The id.
     */
    private long id;

    /**
     * The role.
     */
    private String role;

    /**
     * Gets the user name.
     *
     * @return the user name
     */
    public String getUserName () {
        return userName;
    }

    /**
     * Sets the user name.
     *
     * @param userName
     *         the new user name
     */
    public void setUserName (String userName) {
        this.userName = userName;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getId () {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *         the new id
     */
    public void setId (long id) {
        this.id = id;
    }

    /**
     * Gets the role.
     *
     * @return the role
     */
    public String getRole () {
        return role;
    }

    /**
     * Sets the role.
     *
     * @param role
     *         the new role
     */
    public void setRole (String role) {
        this.role = role;
    }
}
