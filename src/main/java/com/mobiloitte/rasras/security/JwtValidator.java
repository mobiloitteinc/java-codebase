package com.mobiloitte.rasras.security;


import com.mobiloitte.rasras.security.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtValidator.
 */
@Component
class JwtValidator {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtValidator.class);


    /**
     * Validate.
     *
     * @param token
     *         the token
     * @return the jwt user
     */
    public JwtUser validate (String token) {
        String secret = "youtube";
        JwtUser jwtUser = null;
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new JwtUser();

            jwtUser.setUserName(body.getSubject());
            jwtUser.setId(Long.parseLong((String) body.get("userId")));
            jwtUser.setRole((String) body.get("role"));
        } catch (Exception e) {
            LOGGER.error("exception : " + e);

        }

        return jwtUser;
    }
}
