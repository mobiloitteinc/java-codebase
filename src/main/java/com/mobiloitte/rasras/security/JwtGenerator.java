package com.mobiloitte.rasras.security;


import com.mobiloitte.rasras.security.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtGenerator.
 */
@Component
public class JwtGenerator {


    /**
     * Generate.
     *
     * @param jwtUser
     *         the jwt user
     * @return the string
     */
    public String generate (JwtUser jwtUser) {


        Claims claims = Jwts.claims()
                .setSubject(jwtUser.getUserName());
        claims.put("userId", String.valueOf(jwtUser.getId()));
        claims.put("role", jwtUser.getRole());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, "youtube")
                .compact();
    }
}
