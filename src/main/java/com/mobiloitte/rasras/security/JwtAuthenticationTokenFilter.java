package com.mobiloitte.rasras.security;


import com.mobiloitte.rasras.security.model.JwtAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// TODO: Auto-generated Javadoc

/**
 * The Class JwtAuthenticationTokenFilter.
 */
public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {

    /**
     * Instantiates a new jwt authentication token filter.
     */
    public JwtAuthenticationTokenFilter () {
        super("/rest/**");
    }

    /**
     * Attempt authentication.
     *
     * @param httpServletRequest
     *         the http servlet request
     * @param httpServletResponse
     *         the http servlet response
     * @return the authentication
     * @throws AuthenticationException
     *         the authentication exception
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     * @throws ServletException
     *         the servlet exception
     */
    @Override
    public Authentication attemptAuthentication (HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {

        String header = httpServletRequest.getHeader("Authorisation");


        if (header == null || !header.startsWith("Token ")) {
            throw new RuntimeException("JWT Token is missing");
        }

        String authenticationToken = header.substring(6);

        JwtAuthenticationToken token = new JwtAuthenticationToken(authenticationToken);
        return getAuthenticationManager().authenticate(token);
    }


    /**
     * Successful authentication.
     *
     * @param request
     *         the request
     * @param response
     *         the response
     * @param chain
     *         the chain
     * @param authResult
     *         the auth result
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     * @throws ServletException
     *         the servlet exception
     */
    @Override
    protected void successfulAuthentication (HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }
}
