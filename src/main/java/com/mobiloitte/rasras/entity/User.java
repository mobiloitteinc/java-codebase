package com.mobiloitte.rasras.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

// TODO: Auto-generated Javadoc

/**
 * The Class User.
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "userId")
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "user")
public class User {


    /**
     * The user id.
     */
    private Integer userId;

    /**
     * The email.
     */
    private String email;

    /**
     * The mobile number.
     */
    private String mobileNumber;

    /**
     * The password.
     */
    private String password;


    /**
     * The user details.
     */
    private UserDetails userDetails;

    /**
     * The role.
     */
    private Set <Role> role;

    /**
     * The contact us.
     */
    private Set <ContactUs> contactUs;

    /**
     * The devices.
     */
    private Set <Device> devices;


    /**
     * The creation time.
     */
    private Long creationTime = new Date().getTime();

    /**
     * The items.
     */
    private Set <Item> items;

    /**
     * The notification status.
     */
    private Boolean notificationStatus = true;


    /**
     * The status.
     */
    private Boolean status = true;


    private String language = "en";

    /**
     * Gets the notification status.
     *
     * @return the notification status
     */
    @Column(name = "notification_status", columnDefinition = "boolean default true")
    public Boolean getNotificationStatus () {
        return notificationStatus;
    }

    /**
     * Sets the notification status.
     *
     * @param notificationStatus
     *         the new notification status
     */
    public void setNotificationStatus (Boolean notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    @Column(name = "status", columnDefinition = "boolean default true")
    public Boolean getStatus () {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *         the new status
     */
    public void setStatus (Boolean status) {
        this.status = status;
    }

    @Column(name = "language")
    public String getLanguage () {
        return language;
    }

    public void setLanguage (String language) {
        this.language = language;
    }

    /**
     * Gets the user id.
     *
     * @return the user id
     */
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getUserId () {
        return userId;
    }

    /**
     * Sets the user id.
     *
     * @param userId
     *         the new user id
     */
    public void setUserId (Integer userId) {
        this.userId = userId;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    @Size(min = 6, max = 64, message = "The length of email address should be between 6 to 64.")
    @Pattern(message = "Please send valid email address.",
            regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
    @NotNull(message = "Email should not be empty.")
    @Column(name = "email", unique = true)
    public String getEmail () {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email
     *         the new email
     */
    public void setEmail (String email) {
        this.email = email;
    }

    /**
     * Gets the mobile number.
     *
     * @return the mobile number
     */
    //@Pattern(message = "Please send valid 10 digit mobile number.", regexp = "^(\\d{10})$")
    @NotNull(message = "Mobile number should not be empty.")
    @Column(name = "mobile_number", unique = true)
    public String getMobileNumber () {
        return mobileNumber;
    }

    /**
     * Sets the mobile number.
     *
     * @param mobileNumber
     *         the new mobile number
     */
    public void setMobileNumber (String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    @Column(name = "password")
    public String getPassword () {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password
     *         the new password
     */
    public void setPassword (String password) {
        this.password = password;
    }


    /**
     * Gets the user details.
     *
     * @return the user details
     */
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    public UserDetails getUserDetails () {
        return userDetails;
    }

    /**
     * Sets the user details.
     *
     * @param userDetails
     *         the new user details
     */
    public void setUserDetails (UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    /**
     * Gets the role.
     *
     * @return the role
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "user_id", name = "user_role_id")
    public Set <Role> getRole () {
        return role;
    }

    /**
     * Sets the role.
     *
     * @param role
     *         the new role
     */
    public void setRole (Set <Role> role) {
        this.role = role;
    }

    /**
     * Gets the devices.
     *
     * @return the devices
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "user_id", name = "user_device_id")
    public Set <Device> getDevices () {
        return devices;
    }

    /**
     * Sets the devices.
     *
     * @param devices
     *         the new devices
     */
    public void setDevices (Set <Device> devices) {
        this.devices = devices;
    }

    /**
     * Gets the creation time.
     *
     * @return the creation time
     */
    @Column(name = "creation_time")
    public Long getCreationTime () {
        return creationTime;
    }

    /**
     * Sets the creation time.
     *
     * @param creationTime
     *         the new creation time
     */
    public void setCreationTime (Long creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * Gets the items.
     *
     * @return the items
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "user_id", name = "item_user_id")
    public Set <Item> getItems () {
        return items;
    }

    /**
     * Sets the items.
     *
     * @param items
     *         the new items
     */
    public void setItems (Set <Item> items) {
        this.items = items;
    }

    /**
     * Gets the contact us.
     *
     * @return the contact us
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "user_id", name = "user_contact_us_id")
    public Set <ContactUs> getContactUs () {
        return contactUs;
    }

    /**
     * Sets the contact us.
     *
     * @param contactUs
     *         the new contact us
     */
    public void setContactUs (Set <ContactUs> contactUs) {
        this.contactUs = contactUs;
    }


}
