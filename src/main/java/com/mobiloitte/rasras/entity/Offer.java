package com.mobiloitte.rasras.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class Offer.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "offer")
public class Offer {


    /**
     * The offer id.
     */
    private Integer offerId;

    /**
     * The offer on item id.
     */
    private Integer offerOnItemId;

    /**
     * The offer by.
     */
    private Integer offerBy;

    /**
     * The offer to.
     */
    private Integer offerTo;

    /**
     * The offer items.
     */
    private List <OfferItems> offerItems;

    /**
     * The my offer.
     */
    private Boolean myOffer;

    /**
     * The delete status.
     */
    private Integer deleteStatus = 0;

    /**
     * The exchanged item id.
     */
    private Integer exchangedItemId;


    private Integer dealStatus = 0;

    /**
     * Gets the delete status.
     *
     * @return the delete status
     */
    @Column(name = "delete_status")
    public Integer getDeleteStatus () {
        return deleteStatus;
    }

    /**
     * Sets the delete status.
     *
     * @param deleteStatus
     *         the new delete status
     */
    public void setDeleteStatus (Integer deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    /**
     * Gets the offer id.
     *
     * @return the offer id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_id")
    public Integer getOfferId () {
        return offerId;
    }

    /**
     * Sets the offer id.
     *
     * @param offerId
     *         the new offer id
     */
    public void setOfferId (Integer offerId) {
        this.offerId = offerId;
    }

    /**
     * Gets the offer on item id.
     *
     * @return the offer on item id
     */
    @Column(name = "offer_on_item_id")
    public Integer getOfferOnItemId () {
        return offerOnItemId;
    }

    /**
     * Sets the offer on item id.
     *
     * @param offerOnItemId
     *         the new offer on item id
     */
    public void setOfferOnItemId (Integer offerOnItemId) {
        this.offerOnItemId = offerOnItemId;
    }

    /**
     * Gets the offer by.
     *
     * @return the offer by
     */
    @Column(name = "offer_by")
    public Integer getOfferBy () {
        return offerBy;
    }

    /**
     * Sets the offer by.
     *
     * @param offerBy
     *         the new offer by
     */
    public void setOfferBy (Integer offerBy) {
        this.offerBy = offerBy;
    }

    /**
     * Gets the offer to.
     *
     * @return the offer to
     */
    @Column(name = "offer_to")
    public Integer getOfferTo () {
        return offerTo;
    }

    /**
     * Sets the offer to.
     *
     * @param offerTo
     *         the new offer to
     */
    public void setOfferTo (Integer offerTo) {
        this.offerTo = offerTo;
    }

    /**
     * Gets the offer items.
     *
     * @return the offer items
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "offer_id", name = "offer_items_id")
    public List <OfferItems> getOfferItems () {
        return offerItems;
    }

    /**
     * Sets the offer items.
     *
     * @param offerItems
     *         the new offer items
     */
    public void setOfferItems (List <OfferItems> offerItems) {
        this.offerItems = offerItems;
    }

    /**
     * Gets the my offer.
     *
     * @return the my offer
     */
    @Transient

    public Boolean getMyOffer () {
        return myOffer;
    }

    /**
     * Sets the my offer.
     *
     * @param myOffer
     *         the new my offer
     */
    public void setMyOffer (Boolean myOffer) {
        this.myOffer = myOffer;
    }

    /**
     * Gets the exchanged item id.
     *
     * @return the exchanged item id
     */
    @Transient

    public Integer getExchangedItemId () {
        return exchangedItemId;
    }

    /**
     * Sets the exchanged item id.
     *
     * @param exchangedItemId
     *         the new exchanged item id
     */
    public void setExchangedItemId (Integer exchangedItemId) {
        this.exchangedItemId = exchangedItemId;
    }

    @Column(name = "deal_status")
    public Integer getDealStatus () {
        return dealStatus;
    }

    public void setDealStatus (Integer dealStatus) {
        this.dealStatus = dealStatus;
    }
}

