package com.mobiloitte.rasras.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

// TODO: Auto-generated Javadoc

/**
 * The Class Notification.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "notification")
public class Notification {


    /**
     * The notification id.
     */
    private Integer notificationId;

    /**
     * The notification from.
     */
    private Integer notificationFrom;

    /**
     * The notification to.
     */
    private Integer notificationTo;

    /**
     * The notification time.
     */
    private Long notificationTime = new Date().getTime();


    /**
     * The notification title.
     */
    private String notificationTitle;


    /**
     * The notification description.
     */
    private String notificationDescription;


    /**
     * The notification delete status.
     */
    private boolean notificationDeleteStatus = false;


    /**
     * The notification type.
     */
    private String notificationType;


    /**
     * The item id.
     */
    private Integer itemId;


    /**
     * The page number.
     */
    private Integer pageNumber;


    /**
     * The size.
     */
    private Integer size;


    private Integer offerId;

    /**
     * Gets the notification id.
     *
     * @return the notification id
     */
    @Id
    @Column(name = "notification_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getNotificationId () {
        return notificationId;
    }

    /**
     * Sets the notification id.
     *
     * @param notificationId
     *         the new notification id
     */
    public void setNotificationId (Integer notificationId) {
        this.notificationId = notificationId;
    }


    /**
     * Gets the notification from.
     *
     * @return the notification from
     */
    @Column(name = "notification_from")
    public Integer getNotificationFrom () {
        return notificationFrom;
    }

    /**
     * Sets the notification from.
     *
     * @param notificationFrom
     *         the new notification from
     */
    public void setNotificationFrom (Integer notificationFrom) {
        this.notificationFrom = notificationFrom;
    }

    /**
     * Gets the notification to.
     *
     * @return the notification to
     */
    @Column(name = "notification_to")
    public Integer getNotificationTo () {
        return notificationTo;
    }

    /**
     * Sets the notification to.
     *
     * @param notificationTo
     *         the new notification to
     */
    public void setNotificationTo (Integer notificationTo) {
        this.notificationTo = notificationTo;
    }

    /**
     * Gets the notification time.
     *
     * @return the notification time
     */
    @Column(name = "notification_time")
    public Long getNotificationTime () {
        return notificationTime;
    }

    /**
     * Sets the notification time.
     *
     * @param notificationTime
     *         the new notification time
     */
    public void setNotificationTime (Long notificationTime) {
        this.notificationTime = notificationTime;
    }

    /**
     * Gets the notification title.
     *
     * @return the notification title
     */
    @Column(name = "notification_title")
    public String getNotificationTitle () {
        return notificationTitle;
    }

    /**
     * Sets the notification title.
     *
     * @param notificationTitle
     *         the new notification title
     */
    public void setNotificationTitle (String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    /**
     * Gets the notification description.
     *
     * @return the notification description
     */
    @Column(name = "notification_description", columnDefinition = "TEXT")
    public String getNotificationDescription () {
        return notificationDescription;
    }

    /**
     * Sets the notification description.
     *
     * @param notificationDescription
     *         the new notification description
     */
    public void setNotificationDescription (String notificationDescription) {
        this.notificationDescription = notificationDescription;
    }

    /**
     * Checks if is notification delete status.
     *
     * @return true, if is notification delete status
     */
    @Column(name = "notification_delete_status")
    public boolean isNotificationDeleteStatus () {
        return notificationDeleteStatus;
    }

    /**
     * Sets the notification delete status.
     *
     * @param notificationDeleteStatus
     *         the new notification delete status
     */
    public void setNotificationDeleteStatus (boolean notificationDeleteStatus) {
        this.notificationDeleteStatus = notificationDeleteStatus;
    }

    /**
     * Gets the notification type.
     *
     * @return the notification type
     */
    @Column(name = "notification_type")
    public String getNotificationType () {
        return notificationType;
    }

    /**
     * Sets the notification type.
     *
     * @param notificationType
     *         the new notification type
     */
    public void setNotificationType (String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * Gets the item id.
     *
     * @return the item id
     */
    @Column(name = "item_id")
    public Integer getItemId () {
        return itemId;
    }

    /**
     * Sets the item id.
     *
     * @param itemId
     *         the new item id
     */
    public void setItemId (Integer itemId) {
        this.itemId = itemId;
    }


    @Column(name = "offer_id")
    public Integer getOfferId () {
        return offerId;
    }

    public void setOfferId (Integer offerId) {
        this.offerId = offerId;
    }

    /**
     * Gets the page number.
     *
     * @return the page number
     */
    @Transient
    public Integer getPageNumber () {
        return pageNumber;
    }

    /**
     * Sets the page number.
     *
     * @param pageNumber
     *         the new page number
     */
    public void setPageNumber (Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    @Transient
    public Integer getSize () {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size
     *         the new size
     */
    public void setSize (Integer size) {
        this.size = size;
    }
}
