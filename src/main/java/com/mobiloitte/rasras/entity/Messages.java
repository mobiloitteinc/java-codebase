package com.mobiloitte.rasras.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;


// TODO: Auto-generated Javadoc

/**
 * The Class Messages.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "messages")
public class Messages {

    /**
     * The message id.
     */
    private Integer messageId;

    /**
     * The user message id.
     */
    private Integer userMessageId;

    /**
     * The message.
     */
    private String message;

    /**
     * The item message id.
     */
    private Integer itemMessageId;

    /**
     * The to user id.
     */
    private Integer toUserId;

    /**
     * The message time.
     */
    private Long messageTime = new Date().getTime();

    /**
     * The user details.
     */
    private UserDetails userDetails;

    /**
     * The page number.
     */
    private int pageNumber;


    private UserDetails fromUserDetails;


    private UserDetails toUserDetails;

    private Boolean recent = false;

    /**
     * Gets the user details.
     *
     * @return the user details
     */
    @Transient
    public UserDetails getUserDetails () {
        return userDetails;
    }

    /**
     * Sets the user details.
     *
     * @param userDetails
     *         the new user details
     */
    public void setUserDetails (UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    @Transient
    public UserDetails getFromUserDetails () {
        return fromUserDetails;
    }

    public void setFromUserDetails (UserDetails fromUserDetails) {
        this.fromUserDetails = fromUserDetails;
    }

    @Transient
    public UserDetails getToUserDetails () {
        return toUserDetails;
    }

    public void setToUserDetails (UserDetails toUserDetails) {
        this.toUserDetails = toUserDetails;
    }

    /**
     * Gets the message id.
     *
     * @return the message id
     */
    @Id
    @Column(name = "message_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getMessageId () {
        return messageId;
    }

    /**
     * Sets the message id.
     *
     * @param messageId
     *         the new message id
     */
    public void setMessageId (Integer messageId) {
        this.messageId = messageId;
    }

    /**
     * Gets the user message id.
     *
     * @return the user message id
     */
    @Column(name = "user_message_id")
    public Integer getUserMessageId () {
        return userMessageId;
    }

    /**
     * Sets the user message id.
     *
     * @param userMessageId
     *         the new user message id
     */
    public void setUserMessageId (Integer userMessageId) {
        this.userMessageId = userMessageId;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    @Column(name = "message", columnDefinition = "TEXT")
    public String getMessage () {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message
     *         the new message
     */
    public void setMessage (String message) {
        this.message = message;
    }

    /**
     * Gets the item message id.
     *
     * @return the item message id
     */
    @Column(name = "item_message_id")
    public Integer getItemMessageId () {
        return itemMessageId;
    }

    /**
     * Sets the item message id.
     *
     * @param itemMessageId
     *         the new item message id
     */
    public void setItemMessageId (Integer itemMessageId) {
        this.itemMessageId = itemMessageId;
    }

    /**
     * Gets the message time.
     *
     * @return the message time
     */
    @Column(name = "message_time")
    public Long getMessageTime () {
        return messageTime;
    }

    /**
     * Sets the message time.
     *
     * @param messageTime
     *         the new message time
     */
    public void setMessageTime (Long messageTime) {
        this.messageTime = messageTime;
    }

    /**
     * Gets the to user id.
     *
     * @return the to user id
     */
    @Column(name = "to_user_id")
    public Integer getToUserId () {
        return toUserId;
    }

    /**
     * Sets the to user id.
     *
     * @param toUserId
     *         the new to user id
     */
    public void setToUserId (Integer toUserId) {
        this.toUserId = toUserId;
    }

    /**
     * Gets the page number.
     *
     * @return the page number
     */
    @Transient
    public int getPageNumber () {
        return pageNumber;
    }

    /**
     * Sets the page number.
     *
     * @param pageNumber
     *         the new page number
     */
    public void setPageNumber (int pageNumber) {
        this.pageNumber = pageNumber;
    }

    @Column(name = "recent", columnDefinition = "boolean default false")
    public Boolean getRecent () {
        return recent;
    }

    public void setRecent (Boolean recent) {
        this.recent = recent;
    }
}
