package com.mobiloitte.rasras.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

// TODO: Auto-generated Javadoc

/**
 * The Class Like.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "likes")
public class Like {


    /**
     * The like id.
     */
    private Integer likeId;

    /**
     * The liked by.
     */
    private Integer likedBy;

    /**
     * The item owner id.
     */
    private Integer itemOwnerId;

    /**
     * The like time.
     */
    private Long likeTime = new Date().getTime();


    /**
     * The status.
     */
    private String status;

    /**
     * The item like id.
     */
    private Integer itemLikeId;


    /**
     * The like by current user.
     */
    private Boolean likeByCurrentUser;


    /**
     * The dislike by current user.
     */
    private Boolean dislikeByCurrentUser;


    /**
     * The like t.
     */
    private Integer like_t = 0;


    /**
     * Gets the like id.
     *
     * @return the like id
     */
    @Id
    @Column(name = "like_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getLikeId () {
        return likeId;
    }

    /**
     * Sets the like id.
     *
     * @param likeId
     *         the new like id
     */
    public void setLikeId (Integer likeId) {
        this.likeId = likeId;
    }

    /**
     * Gets the liked by.
     *
     * @return the liked by
     */
    @Column(name = "liked_by")
    public Integer getLikedBy () {
        return likedBy;
    }

    /**
     * Sets the liked by.
     *
     * @param likedBy
     *         the new liked by
     */
    public void setLikedBy (Integer likedBy) {
        this.likedBy = likedBy;
    }

    /**
     * Gets the item owner id.
     *
     * @return the item owner id
     */
    @Column(name = "item_owner_id")
    public Integer getItemOwnerId () {
        return itemOwnerId;
    }

    /**
     * Sets the item owner id.
     *
     * @param itemOwnerId
     *         the new item owner id
     */
    public void setItemOwnerId (Integer itemOwnerId) {
        this.itemOwnerId = itemOwnerId;
    }

    /**
     * Gets the like time.
     *
     * @return the like time
     */
    @Column(name = "like_time")
    public Long getLikeTime () {
        return likeTime;
    }

    /**
     * Sets the like time.
     *
     * @param likeTime
     *         the new like time
     */
    public void setLikeTime (Long likeTime) {
        this.likeTime = likeTime;
    }

    /**
     * Gets the item like id.
     *
     * @return the item like id
     */
    @Column(name = "item_like_id")
    public Integer getItemLikeId () {
        return itemLikeId;
    }

    /**
     * Sets the item like id.
     *
     * @param itemLikeId
     *         the new item like id
     */
    public void setItemLikeId (Integer itemLikeId) {
        this.itemLikeId = itemLikeId;
    }


    /**
     * Gets the like by current user.
     *
     * @return the like by current user
     */
    @Transient
    public Boolean getLikeByCurrentUser () {
        return likeByCurrentUser;
    }

    /**
     * Sets the like by current user.
     *
     * @param likeByCurrentUser
     *         the new like by current user
     */
    public void setLikeByCurrentUser (Boolean likeByCurrentUser) {
        this.likeByCurrentUser = likeByCurrentUser;
    }

    /**
     * Gets the dislike by current user.
     *
     * @return the dislike by current user
     */
    @Transient
    public Boolean getDislikeByCurrentUser () {
        return dislikeByCurrentUser;
    }

    /**
     * Sets the dislike by current user.
     *
     * @param dislikeByCurrentUser
     *         the new dislike by current user
     */
    public void setDislikeByCurrentUser (Boolean dislikeByCurrentUser) {
        this.dislikeByCurrentUser = dislikeByCurrentUser;
    }

    /**
     * Gets the like t.
     *
     * @return the like t
     */
    @Column(name = "like_t")
    public Integer getLike_t () {
        return like_t;
    }

    /**
     * Sets the like t.
     *
     * @param like_t
     *         the new like t
     */
    public void setLike_t (Integer like_t) {
        this.like_t = like_t;
    }


    /**
     * Gets the status.
     *
     * @return the status
     */
    @Transient
    public String getStatus () {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *         the new status
     */
    public void setStatus (String status) {
        this.status = status;
    }
}
