package com.mobiloitte.rasras.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc

/**
 * The Class OfferItems.
 */
@Entity
@Table(name = "offer_items")
public class OfferItems {

    /**
     * The offer item id.
     */
    private Integer offerItemId;

    /**
     * The item id.
     */
    private Integer itemId;

    /**
     * The offer id.
     */
    private Integer offerId;

    /**
     * Gets the offer item id.
     *
     * @return the offer item id
     */
    @Id
    @Column(name = "offer_item_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getOfferItemId () {
        return offerItemId;
    }

    /**
     * Sets the offer item id.
     *
     * @param offerItemId
     *         the new offer item id
     */
    public void setOfferItemId (Integer offerItemId) {
        this.offerItemId = offerItemId;
    }

    /**
     * Gets the item id.
     *
     * @return the item id
     */
    @Column(name = "item_id")
    public Integer getItemId () {
        return itemId;
    }

    /**
     * Sets the item id.
     *
     * @param itemId
     *         the new item id
     */
    public void setItemId (Integer itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the offer id.
     *
     * @return the offer id
     */
    @Column(name = "offer_items_id")
    public Integer getOfferId () {
        return offerId;
    }

    /**
     * Sets the offer id.
     *
     * @param offerId
     *         the new offer id
     */
    public void setOfferId (Integer offerId) {
        this.offerId = offerId;
    }


}
