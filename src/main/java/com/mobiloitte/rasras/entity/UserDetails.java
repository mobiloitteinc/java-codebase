package com.mobiloitte.rasras.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc

/**
 * The Class UserDetails.
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "userDetailsId")
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "user_detail")
public class UserDetails {


    /**
     * The user details id.
     */
    private Integer userDetailsId;

    /**
     * The first name.
     */
    private String firstName;

    /**
     * The last name.
     */
    private String lastName;

    /**
     * The date of birth.
     */
    private String dateOfBirth;

    /**
     * The gender.
     */
    private String gender;

    /**
     * The address.
     */
    private String address;

    /**
     * The biography.
     */
    private String biography;

    /**
     * The image.
     */
    private String image = "https://res.cloudinary.com/dmd2a0bgi/image/upload/v1511952431/jrbjbm6nnrso7pfykjsu.jpg";

    /**
     * The user.
     */
    private User user;

    /**
     * The latitude.
     */
    private Double latitude;

    /**
     * The longitude.
     */
    private Double longitude;


    /**
     * Gets the latitude.
     *
     * @return the latitude
     */
    @Column(name = "latitude")
    public Double getLatitude () {
        return latitude;
    }


    /**
     * Sets the latitude.
     *
     * @param latitude
     *         the new latitude
     */
    public void setLatitude (Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets the longitude.
     *
     * @return the longitude
     */
    @Column(name = "longitude")
    public Double getLongitude () {
        return longitude;
    }

    /**
     * Sets the longitude.
     *
     * @param longitude
     *         the new longitude
     */
    public void setLongitude (Double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the user details id.
     *
     * @return the user details id
     */
    @Id
    @Column(name = "user_details_id")
    @GeneratedValue(generator = "gen")
    @GenericGenerator(name = "gen", strategy = "foreign", parameters = @org.hibernate.annotations.Parameter(name = "property", value = "user"))

    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getUserDetailsId () {
        return userDetailsId;
    }

    /**
     * Sets the user details id.
     *
     * @param userDetailsId
     *         the new user details id
     */
    public void setUserDetailsId (Integer userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    /**
     * Gets the first name.
     *
     * @return the first name
     */
    @Column(name = "first_name")
    public String getFirstName () {
        return firstName;
    }

    /**
     * Sets the first name.
     *
     * @param firstName
     *         the new first name
     */
    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name.
     *
     * @return the last name
     */
    @Column(name = "last_name")
    public String getLastName () {
        return lastName;
    }

    /**
     * Sets the last name.
     *
     * @param lastName
     *         the new last name
     */
    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the date of birth.
     *
     * @return the date of birth
     */
    @Column(name = "dob")
    public String getDateOfBirth () {
        return dateOfBirth;
    }

    /**
     * Sets the date of birth.
     *
     * @param dateOfBirth
     *         the new date of birth
     */
    public void setDateOfBirth (String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets the gender.
     *
     * @return the gender
     */
    @Column(name = "gender")
    public String getGender () {
        return gender;
    }

    /**
     * Sets the gender.
     *
     * @param gender
     *         the new gender
     */
    public void setGender (String gender) {
        this.gender = gender;
    }

    /**
     * Gets the address.
     *
     * @return the address
     */
    @Column(name = "address", columnDefinition = "TEXT")
    public String getAddress () {
        return address;
    }

    /**
     * Sets the address.
     *
     * @param address
     *         the new address
     */
    public void setAddress (String address) {
        this.address = address;
    }


    /**
     * Gets the biography.
     *
     * @return the biography
     */
    @Column(name = "biography")
    public String getBiography () {
        return biography;
    }

    /**
     * Sets the biography.
     *
     * @param biography
     *         the new biography
     */
    public void setBiography (String biography) {
        this.biography = biography;
    }

    /**
     * Gets the image.
     *
     * @return the image
     */
    @Column(name = "image")
    public String getImage () {
        return image;
    }

    /**
     * Sets the image.
     *
     * @param image
     *         the new image
     */
    public void setImage (String image) {
        this.image = image;
    }


    /**
     * Gets the user.
     *
     * @return the user
     */
    @JoinColumn(name = "user_details_id", referencedColumnName = "user_id")
    @OneToOne(optional = false)
    public User getUser () {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user
     *         the new user
     */
    public void setUser (User user) {
        this.user = user;
    }
}
