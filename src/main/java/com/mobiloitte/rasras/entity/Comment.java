package com.mobiloitte.rasras.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

// TODO: Auto-generated Javadoc

/**
 * The Class Comment.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "comments")
public class Comment {

    /**
     * The comment id.
     */
    private Integer commentId;

    /**
     * The comment.
     */
    private String comment;

    /**
     * The comment by.
     */
    private Integer commentBy;

    /**
     * The item comment id.
     */
    private Integer itemCommentId;

    /**
     * The comment time.
     */
    private Long commentTime = new Date().getTime();

    /**
     * The user.
     */
    private UserDetails user;

    /**
     * Gets the comment id.
     *
     * @return the comment id
     */
    @Id
    @Column(name = "comment_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getCommentId () {
        return commentId;
    }

    /**
     * Sets the comment id.
     *
     * @param commentId
     *         the new comment id
     */
    public void setCommentId (Integer commentId) {
        this.commentId = commentId;
    }

    /**
     * Gets the comment.
     *
     * @return the comment
     */
    @Column(name = "comment", columnDefinition = "TEXT")
    public String getComment () {
        return comment;
    }

    /**
     * Sets the comment.
     *
     * @param comment
     *         the new comment
     */
    public void setComment (String comment) {
        this.comment = comment;
    }

    /**
     * Gets the comment by.
     *
     * @return the comment by
     */
    @Column(name = "comment_by")
    public Integer getCommentBy () {
        return commentBy;
    }

    /**
     * Sets the comment by.
     *
     * @param commentBy
     *         the new comment by
     */
    public void setCommentBy (Integer commentBy) {
        this.commentBy = commentBy;
    }

    /**
     * Gets the item comment id.
     *
     * @return the item comment id
     */
    @Column(name = "item_comment_id")
    public Integer getItemCommentId () {
        return itemCommentId;
    }

    /**
     * Sets the item comment id.
     *
     * @param itemCommentId
     *         the new item comment id
     */
    public void setItemCommentId (Integer itemCommentId) {
        this.itemCommentId = itemCommentId;
    }

    /**
     * Gets the comment time.
     *
     * @return the comment time
     */
    @Column(name = "comment_time")
    public Long getCommentTime () {
        return commentTime;
    }

    /**
     * Sets the comment time.
     *
     * @param commentTime
     *         the new comment time
     */
    public void setCommentTime (Long commentTime) {
        this.commentTime = commentTime;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    @Transient
    public UserDetails getUser () {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user
     *         the new user
     */
    public void setUser (UserDetails user) {
        this.user = user;
    }
}
