package com.mobiloitte.rasras.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class Item.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "item")
public class Item {

    /**
     * The item id.
     */
    private Integer itemId;

    /**
     * The item title.
     */
    private String itemTitle;

    /**
     * The year of purchase.
     */
    private String yearOfPurchase;

    /**
     * The material.
     */
    private String material;

    /**
     * The city.
     */
    private String city;

    /**
     * The pick up address.
     */
    private String pickUpAddress;

    /**
     * The contact number.
     */
    private String contactNumber;

    /**
     * The item tags.
     */
    private String itemTags;

    /**
     * The item description.
     */
    private String itemDescription;

    /**
     * The item creation time.
     */
    private Long itemCreationTime = new Date().getTime();


    /**
     * The item images.
     */
    private List <Image> itemImages;

    /**
     * The item likes.
     */
    private List <Like> itemLikes;


    /**
     * The item comments.
     */
    private List <Comment> itemComments;


    /**
     * The item category id.
     */
    private Integer itemCategoryId;

    /**
     * The color id.
     */
    private Integer colorId;

    /**
     * The size id.
     */
    private Integer sizeId;

    /**
     * The item like count.
     */
    private Integer itemLikeCount = 0;

    /**
     * The item comment count.
     */
    private Integer itemCommentCount = 0;

    /**
     * The user item id.
     */
    private Integer userItemId;

    /**
     * The latitude.
     */
    private Double latitude;

    /**
     * The longitude.
     */
    private Double longitude;


    /**
     * The name.
     */
    private String name;


    /**
     * The image URL.
     */
    private String imageURL;

    /**
     * The like by current user.
     */
    private Boolean likeByCurrentUser;


    /**
     * The dislike by current user.
     */
    private Boolean dislikeByCurrentUser;

    /**
     * The item image.
     */
    private Image itemImage;

    /**
     * The delete status.
     */
    private Integer deleteStatus = 0;

    /**
     * The page number.
     */
    private Integer pageNumber;

    /**
     * Gets the delete status.
     *
     * @return the delete status
     */
    @Column(name = "delete_status")
    public Integer getDeleteStatus () {
        return deleteStatus;
    }

    /**
     * Sets the delete status.
     *
     * @param deleteStatus
     *         the new delete status
     */
    public void setDeleteStatus (Integer deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    /**
     * Gets the item id.
     *
     * @return the item id
     */
    @Id
    @Column(name = "item_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getItemId () {
        return itemId;
    }

    /**
     * Sets the item id.
     *
     * @param itemId
     *         the new item id
     */
    public void setItemId (Integer itemId) {
        this.itemId = itemId;
    }

    /**
     * Gets the item title.
     *
     * @return the item title
     */
    @Column(name = "item_title", columnDefinition = "TEXT")
    public String getItemTitle () {
        return itemTitle;
    }

    /**
     * Sets the item title.
     *
     * @param itemTitle
     *         the new item title
     */
    public void setItemTitle (String itemTitle) {
        this.itemTitle = itemTitle;
    }

    /**
     * Gets the year of purchase.
     *
     * @return the year of purchase
     */
    @Column(name = "year_of_purchase")
    public String getYearOfPurchase () {
        return yearOfPurchase;
    }

    /**
     * Sets the year of purchase.
     *
     * @param yearOfPurchase
     *         the new year of purchase
     */
    public void setYearOfPurchase (String yearOfPurchase) {
        this.yearOfPurchase = yearOfPurchase;
    }

    /**
     * Gets the material.
     *
     * @return the material
     */
    @Column(name = "material")
    public String getMaterial () {
        return material;
    }

    /**
     * Sets the material.
     *
     * @param material
     *         the new material
     */
    public void setMaterial (String material) {
        this.material = material;
    }

    /**
     * Gets the city.
     *
     * @return the city
     */
    @Column(name = "city")
    public String getCity () {
        return city;
    }

    /**
     * Sets the city.
     *
     * @param city
     *         the new city
     */
    public void setCity (String city) {
        this.city = city;
    }

    /**
     * Gets the pick up address.
     *
     * @return the pick up address
     */
    @Column(name = "pick_up_address", columnDefinition = "TEXT")
    public String getPickUpAddress () {
        return pickUpAddress;
    }

    /**
     * Sets the pick up address.
     *
     * @param pickUpAddress
     *         the new pick up address
     */
    public void setPickUpAddress (String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    /**
     * Gets the contact number.
     *
     * @return the contact number
     */
    @Column(name = "contact_number")
    public String getContactNumber () {
        return contactNumber;
    }

    /**
     * Sets the contact number.
     *
     * @param contactNumber
     *         the new contact number
     */
    public void setContactNumber (String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     * Gets the item tags.
     *
     * @return the item tags
     */
    @Column(name = "item_tags")
    public String getItemTags () {
        return itemTags;
    }

    /**
     * Sets the item tags.
     *
     * @param itemTags
     *         the new item tags
     */
    public void setItemTags (String itemTags) {
        this.itemTags = itemTags;
    }

    /**
     * Gets the item description.
     *
     * @return the item description
     */
    @Column(name = "item_description", columnDefinition = "TEXT")
    public String getItemDescription () {
        return itemDescription;
    }

    /**
     * Sets the item description.
     *
     * @param itemDescription
     *         the new item description
     */
    public void setItemDescription (String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     * Gets the item creation time.
     *
     * @return the item creation time
     */
    @Column(name = "item_creation_time")
    public Long getItemCreationTime () {
        return itemCreationTime;
    }

    /**
     * Sets the item creation time.
     *
     * @param itemCreationTime
     *         the new item creation time
     */
    public void setItemCreationTime (Long itemCreationTime) {
        this.itemCreationTime = itemCreationTime;
    }

    /**
     * Gets the item images.
     *
     * @return the item images
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "item_id", name = "item_image_id")
    public List <Image> getItemImages () {
        return itemImages;
    }

    /**
     * Sets the item images.
     *
     * @param itemImages
     *         the new item images
     */
    public void setItemImages (List <Image> itemImages) {
        this.itemImages = itemImages;
    }


    /**
     * Gets the item likes.
     *
     * @return the item likes
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "item_id", name = "item_like_id")
    public List <Like> getItemLikes () {
        return itemLikes;
    }

    /**
     * Sets the item likes.
     *
     * @param itemLikes
     *         the new item likes
     */
    public void setItemLikes (List <Like> itemLikes) {
        this.itemLikes = itemLikes;
    }

    /**
     * Gets the item comments.
     *
     * @return the item comments
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "item_id", name = "item_comment_id")
    public List <Comment> getItemComments () {
        return itemComments;
    }

    /**
     * Sets the item comments.
     *
     * @param itemComments
     *         the new item comments
     */
    public void setItemComments (List <Comment> itemComments) {
        this.itemComments = itemComments;
    }


    /**
     * Gets the item category id.
     *
     * @return the item category id
     */
    @Column(name = "item_category_id")
    public Integer getItemCategoryId () {
        return itemCategoryId;
    }

    /**
     * Sets the item category id.
     *
     * @param itemCategoryId
     *         the new item category id
     */
    public void setItemCategoryId (Integer itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    /**
     * Gets the color id.
     *
     * @return the color id
     */
    @Column(name = "item_color_id")
    public Integer getColorId () {
        return colorId;
    }

    /**
     * Sets the color id.
     *
     * @param colorId
     *         the new color id
     */
    public void setColorId (Integer colorId) {
        this.colorId = colorId;
    }

    /**
     * Gets the size id.
     *
     * @return the size id
     */
    @Column(name = "item_size_id")
    public Integer getSizeId () {
        return sizeId;
    }

    /**
     * Sets the size id.
     *
     * @param sizeId
     *         the new size id
     */
    public void setSizeId (Integer sizeId) {
        this.sizeId = sizeId;
    }

    /**
     * Gets the item like count.
     *
     * @return the item like count
     */
    @Column(name = "item_like_count")
    public Integer getItemLikeCount () {
        return itemLikeCount;
    }

    /**
     * Sets the item like count.
     *
     * @param itemLikeCount
     *         the new item like count
     */
    public void setItemLikeCount (Integer itemLikeCount) {
        this.itemLikeCount = itemLikeCount;
    }

    /**
     * Gets the item comment count.
     *
     * @return the item comment count
     */
    @Column(name = "item_comment_count")
    public Integer getItemCommentCount () {
        return itemCommentCount;
    }

    /**
     * Sets the item comment count.
     *
     * @param itemCommentCount
     *         the new item comment count
     */
    public void setItemCommentCount (Integer itemCommentCount) {
        this.itemCommentCount = itemCommentCount;
    }


    /**
     * Gets the user item id.
     *
     * @return the user item id
     */
    @Column(name = "item_user_id")
    public Integer getUserItemId () {
        return userItemId;
    }

    /**
     * Sets the user item id.
     *
     * @param userItemId
     *         the new user item id
     */
    public void setUserItemId (Integer userItemId) {
        this.userItemId = userItemId;
    }


    /**
     * Gets the latitude.
     *
     * @return the latitude
     */
    @Column(name = "latitude")
    public Double getLatitude () {
        return latitude;
    }

    /**
     * Sets the latitude.
     *
     * @param latitude
     *         the new latitude
     */
    public void setLatitude (Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets the longitude.
     *
     * @return the longitude
     */
    @Column(name = "longitude")
    public Double getLongitude () {
        return longitude;
    }

    /**
     * Sets the longitude.
     *
     * @param longitude
     *         the new longitude
     */
    public void setLongitude (Double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the like by current user.
     *
     * @return the like by current user
     */
    @Transient
    public Boolean getLikeByCurrentUser () {
        return likeByCurrentUser;
    }

    /**
     * Sets the like by current user.
     *
     * @param likeByCurrentUser
     *         the new like by current user
     */
    public void setLikeByCurrentUser (Boolean likeByCurrentUser) {
        this.likeByCurrentUser = likeByCurrentUser;
    }

    /**
     * Gets the dislike by current user.
     *
     * @return the dislike by current user
     */
    @Transient
    public Boolean getDislikeByCurrentUser () {
        return dislikeByCurrentUser;
    }

    /**
     * Sets the dislike by current user.
     *
     * @param dislikeByCurrentUser
     *         the new dislike by current user
     */
    public void setDislikeByCurrentUser (Boolean dislikeByCurrentUser) {
        this.dislikeByCurrentUser = dislikeByCurrentUser;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @Transient
    public String getName () {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *         the new name
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * Gets the image URL.
     *
     * @return the image URL
     */
    @Transient
    public String getImageURL () {
        return imageURL;
    }

    /**
     * Sets the image URL.
     *
     * @param imageURL
     *         the new image URL
     */
    public void setImageURL (String imageURL) {
        this.imageURL = imageURL;
    }


    /**
     * Gets the item image.
     *
     * @return the item image
     */
    @Transient
    public Image getItemImage () {
        return itemImage;
    }

    /**
     * Sets the item image.
     *
     * @param itemImage
     *         the new item image
     */
    public void setItemImage (Image itemImage) {
        this.itemImage = itemImage;
    }

    /**
     * Gets the page number.
     *
     * @return the page number
     */
    @Transient
    public Integer getPageNumber () {
        return pageNumber;
    }

    /**
     * Sets the page number.
     *
     * @param pageNumber
     *         the new page number
     */
    public void setPageNumber (Integer pageNumber) {
        this.pageNumber = pageNumber;
    }
}
