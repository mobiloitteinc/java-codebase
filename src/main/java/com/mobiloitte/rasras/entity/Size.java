package com.mobiloitte.rasras.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

// TODO: Auto-generated Javadoc

/**
 * The Class Size.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "size")
public class Size {

    /**
     * The size id.
     */
    private Integer sizeId;

    /**
     * The size.
     */
    private String size;

    /**
     * The item size.
     */
    private Set <Item> itemSize;


    /**
     * Gets the size id.
     *
     * @return the size id
     */
    @Id
    @Column(name = "size_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getSizeId () {
        return sizeId;
    }

    /**
     * Sets the size id.
     *
     * @param sizeId
     *         the new size id
     */
    public void setSizeId (Integer sizeId) {
        this.sizeId = sizeId;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    @Column(name = "item_size")
    public String getSize () {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size
     *         the new size
     */
    public void setSize (String size) {
        this.size = size;
    }

    /**
     * Gets the item size.
     *
     * @return the item size
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "size_id", name = "item_size_id")
    public Set <Item> getItemSize () {
        return itemSize;
    }

    /**
     * Sets the item size.
     *
     * @param itemSize
     *         the new item size
     */
    public void setItemSize (Set <Item> itemSize) {
        this.itemSize = itemSize;
    }


}
