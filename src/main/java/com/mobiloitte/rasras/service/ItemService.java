package com.mobiloitte.rasras.service;

import com.mobiloitte.rasras.entity.Comment;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.Like;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Interface ItemService.
 */
public interface ItemService {

    /**
     * Save.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> save (Item item, HttpServletRequest request) throws Exception;

    /**
     * Gets the item.
     *
     * @param request
     *         the request
     * @return the item
     * @throws Exception
     *         the exception
     */
    Map <String, Object> getItem (HttpServletRequest request) throws Exception;

    /**
     * Gets the item.
     *
     * @param categoryId
     *         the category id
     * @param request
     *         the request
     * @return the item
     * @throws Exception
     *         the exception
     */
    Map <String, Object> getItem (int categoryId, HttpServletRequest request) throws Exception;

    /**
     * Adds the like.
     *
     * @param like
     *         the like
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> addLike (Like like, HttpServletRequest request) throws Exception;

    /**
     * Adds the comment.
     *
     * @param comment
     *         the comment
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> addComment (Comment comment, HttpServletRequest request) throws Exception;

    /**
     * Gets the item detail.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the item detail
     * @throws Exception
     *         the exception
     */
    Map <String, Object> getItemDetail (Item item, HttpServletRequest request) throws Exception;

    /**
     * Gets the comments.
     *
     * @param itemId
     *         the item id
     * @param request
     *         the request
     * @param pageNumber
     *         the page number
     * @return the comments
     * @throws Exception
     *         the exception
     */
    Map <String, Object> getComments (int itemId, HttpServletRequest request, int pageNumber) throws Exception;

    /**
     * Gets the home items.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the home items
     * @throws Exception
     *         the exception
     */
    Map <String, Object> getHomeItems (Item item, HttpServletRequest request) throws Exception;

    /**
     * Gets the favourite items.
     *
     * @param request
     *         the request
     * @return the favourite items
     * @throws Exception
     *         the exception
     */
    Map <String, Object> getFavouriteItems (Item item, HttpServletRequest request) throws Exception;

    /**
     * Search.
     *
     * @param item
     *         the item
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> search (Item item, HttpServletRequest request) throws Exception;

    Map <String, Object> deleteItem (int itemId, HttpServletRequest request) throws Exception;
}
