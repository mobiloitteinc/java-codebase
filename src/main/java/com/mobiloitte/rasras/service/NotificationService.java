package com.mobiloitte.rasras.service;

import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Interface NotificationService.
 */
public interface NotificationService {

    /**
     * Gets the notifications.
     *
     * @param notificationManager
     *         the notification manager
     * @param request
     *         the request
     * @return the notifications
     * @throws Exception
     *         the exception
     */
    HashMap <String, Object> getNotifications (Notification notificationManager, HttpServletRequest request) throws Exception;

    /**
     * Sets the notification status.
     *
     * @param user
     *         the user
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> setNotificationStatus (User user, HttpServletRequest request) throws Exception;

    /**
     * Gets the notification.
     *
     * @param id
     *         the id
     * @param request
     *         the request
     * @return the notification
     * @throws Exception
     *         the exception
     */
    Map <String, Object> getNotification (int id, HttpServletRequest request) throws Exception;


    Map <String, Object> delete (int id, HttpServletRequest request) throws Exception;
}
