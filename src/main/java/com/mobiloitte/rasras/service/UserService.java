package com.mobiloitte.rasras.service;


import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.model.ChangePassword;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


// TODO: Auto-generated Javadoc

/**
 * The Interface UserService.
 */
public interface UserService {

    /**
     * Gets the.
     *
     * @param userId
     *         the user id
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> get (Integer userId, HttpServletRequest request) throws Exception;

    /**
     * Change.
     *
     * @param changePassword
     *         the change password
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> change (ChangePassword changePassword, HttpServletRequest request) throws Exception;

    /**
     * Update.
     *
     * @param userDetails
     *         the user details
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> update (UserDetails userDetails, HttpServletRequest request) throws Exception;

    /**
     * Verify.
     *
     * @param otp
     *         the otp
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> verify (OTP otp, HttpServletRequest request) throws Exception;

    /**
     * Reset password.
     *
     * @param changePassword
     *         the change password
     * @param request
     *         the request
     * @return the map
     * @throws Exception
     *         the exception
     */
    Map <String, Object> resetPassword (ChangePassword changePassword, HttpServletRequest request) throws Exception;
}
