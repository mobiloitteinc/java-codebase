package com.mobiloitte.rasras;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

// TODO: Auto-generated Javadoc

/**
 * The Class RasrasApplication.
 */
@SpringBootApplication
public class RasrasApplication extends SpringBootServletInitializer {


    /**
     */
    public static void main (String[] args) {


        SpringApplication.run(RasrasApplication.class, args);

    }

    /**
     * Configure.
     *
     * @param application
     *         the application
     * @return the spring application builder
     */
    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder application) {


        return application.sources(RasrasApplication.class);
    }

    /**
     * Started.
     */
    @PostConstruct
    void started () {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }


}
