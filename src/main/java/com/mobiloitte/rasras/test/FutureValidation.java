package com.mobiloitte.rasras.test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FutureValidation {

    public boolean futureValidation (String date) {
        try {
            if (new SimpleDateFormat("dd/MM/yyyy").parse(date).after(new Date())) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
