package com.mobiloitte.rasras.test;

public class SizeValidator {

    public boolean sizeValidation (int min, int max, String data) {
        return data.length() >= min && data.length() <= max;
    }

}
