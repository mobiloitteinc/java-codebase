package com.mobiloitte.rasras.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private final Pattern pattern;

    public Validator () {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    /**
     * Validate hex with regular expression
     *
     * @param hex
     *         hex for validation
     * @return true valid hex, false invalid hex
     */
    public boolean validate (final String hex) {

        Matcher matcher = pattern.matcher(hex);
        return matcher.matches();

    }

    public boolean validatePhoneNumber (String phoneNo) {
        if (phoneNo.matches("\\d{10}"))
            return true;
        else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}"))
            return true;
        else
            return phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}") || phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}");

    }

    public boolean passwordValidation (String password) {
        return password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
    }

    public boolean textValidation (String name) {
        String regex = "^[\\p{L} .'-]+$";
        return name.length() >= 3 && name.length() < 31 || name.matches(regex);
    }
}