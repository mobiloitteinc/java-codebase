package com.mobiloitte.rasras.test;

public class ZipcodeValidationUk {

    public boolean zipCodeValidationForUk (String data) {
        String pattern = "\\b[A-Z0-9]+\\b";

        if (data.matches(pattern)) {
            return true;
        } else if (data.length() == 6) {
            return true;
        }
        return false;
    }

}
