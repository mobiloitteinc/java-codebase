package com.mobiloitte.rasras.test;

public class SearchValidator {

    public boolean searchValidation (String data) {
        String regex = "^[\\p{L} .'-]+$";
        if (data.matches(regex)) {
            return true;
        } else if (data.length() >= 3 && data.length() <= 12) {
            return true;
        }
        return false;
    }

}
