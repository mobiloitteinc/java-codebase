package com.mobiloitte.rasras.test;

public class PostalCodeValidationAustralia {

    public boolean postalCodeValidationAustralia (String postalCode) {

        String pattern = "\\d{5,10}";

        return postalCode.matches(pattern);
    }
}
