package com.mobiloitte.rasras.test;

public class ZipCodeValidatior {

    public boolean zipCodeValidation (String zipCode) {
        String pattern = "\\d{5,10}";
        return zipCode.matches(pattern);
    }

}
