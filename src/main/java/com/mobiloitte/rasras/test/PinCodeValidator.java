package com.mobiloitte.rasras.test;

public class PinCodeValidator {

    public boolean pinCodeValidation (String zipCode) {
        String pattern = "\\d{6,10}";
        return zipCode.matches(pattern);
    }
}
