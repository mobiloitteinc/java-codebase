package com.mobiloitte.rasras.test;

public class MobileValidator {

    public boolean validatePhoneNumber (String phoneNo) {
        if (phoneNo.matches("\\d{10}"))
            return true;
        else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}"))
            return true;
        else
            return phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}") || phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}");

    }

}
