package com.mobiloitte.rasras.test;

public class PasswordValidator {

    public boolean passwordValidation (String password) {
        return password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
    }
}
