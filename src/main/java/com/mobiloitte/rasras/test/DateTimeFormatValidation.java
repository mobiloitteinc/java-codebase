package com.mobiloitte.rasras.test;

public class DateTimeFormatValidation {

    public boolean dateTimeformatValidationinYYYYMMDD (String date) {
        String regex = "\\d{4}-\\d{2}-\\d{2}";

        return date.matches(regex);

    }

    public boolean dateTimeformatValidationinDDMMYYYY (String date) {
        String regex = "\\d{2}-\\d{2}-\\d{4}";

        return date.matches(regex);

    }

}
