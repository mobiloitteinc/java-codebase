package com.mobiloitte.rasras.test;

public class NotEmptyValidator {

    public boolean notEmptyValidation (String data) {
        return !data.isEmpty();
    }

}
