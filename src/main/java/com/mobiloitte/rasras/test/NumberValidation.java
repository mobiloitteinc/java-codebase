package com.mobiloitte.rasras.test;

public class NumberValidation {

    public boolean numberValidation (String data) {
        String regex = "\\d+";
        return data.matches(regex);
    }

}
