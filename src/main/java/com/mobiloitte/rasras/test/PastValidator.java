package com.mobiloitte.rasras.test;


import java.text.SimpleDateFormat;
import java.util.Date;

public class PastValidator {
    public boolean pastValidation (String date) {

        try {
            if (new SimpleDateFormat("dd/MM/yyyy").parse(date).before(new Date())) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
