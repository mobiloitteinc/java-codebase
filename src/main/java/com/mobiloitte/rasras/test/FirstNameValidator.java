package com.mobiloitte.rasras.test;

public class FirstNameValidator {

    public boolean textValidation (String name) {
        String regex = "^[\\p{L} .'-]+$";
        return name.length() >= 3 && name.length() < 31 || name.matches(regex);
    }

}
