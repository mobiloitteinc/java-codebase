package com.mobiloitte.rasras.dao;

import com.mobiloitte.rasras.entity.Comment;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.Like;
import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.Offer;
import com.mobiloitte.rasras.entity.Size;

import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc

/**
 * The Interface ItemDao.
 */
public interface ItemDao {

    /**
     * Save.
     *
     * @param item
     *         the item
     * @return the int
     * @throws Exception
     *         the exception
     */
    int save (Item item);

    /**
     * Gets the size.
     *
     * @return the size
     * @throws Exception
     *         the exception
     */
    List <Size> getSize ();

    /**
     * Gets the color.
     *
     * @return the color
     * @throws Exception
     *         the exception
     */
    List <Color> getColor ();

    /**
     * Gets the category.
     *
     * @return the category
     * @throws Exception
     *         the exception
     */
    List <Category> getCategory ();

    /**
     * Gets the item.
     *
     * @param categoryId
     *         the category id
     * @return the item
     * @throws Exception
     *         the exception
     */
    List <Item> getItem (int categoryId);

    /**
     * Adds the like.
     *
     * @param like
     *         the like
     * @return the integer
     * @throws Exception
     *         the exception
     */
    void addLike (Like like);

    /**
     * Update like.
     *
     * @param activityLikeDislike
     *         the activity like dislike
     * @return the int
     * @throws Exception
     *         the exception
     */
    int updateLike (Like activityLikeDislike);

    /**
     * Adds the like count.
     *
     * @param itemId
     *         the item id
     * @return the int
     * @throws Exception
     *         the exception
     */
    void addLikeCount (int itemId);

    /**
     * Subtract like count.
     *
     * @param itemId
     *         the item id
     * @return the int
     * @throws Exception
     *         the exception
     */
    void subtractLikeCount (int itemId);

    /**
     * Adds the comment.
     *
     * @param comment
     *         the comment
     * @return the int
     * @throws Exception
     *         the exception
     */
    int addComment (Comment comment);

    /**
     * Gets the item detail.
     *
     * @param item
     *         the item
     * @return the item detail
     * @throws Exception
     *         the exception
     */
    Map getItemDetail (Item item);

    /**
     * Adds the comment count.
     *
     * @param itemId
     *         the item id
     * @return the int
     * @throws Exception
     *         the exception
     */
    void addCommentCount (int itemId);

    /**
     * Gets the comments.
     *
     * @param itemId
     *         the item id
     * @param pageNumber
     *         the page number
     * @return the comments
     * @throws Exception
     *         the exception
     */
    List getComments (int itemId, int pageNumber);

    /**
     * Gets the home items.
     *
     * @param item
     *         the item
     * @return the home items
     */
    List getHomeItems (Item item);

    /**
     * Gets the item images.
     *
     * @param itemId
     *         the item id
     * @return the item images
     */
    List getItemImages (int itemId);

    /**
     * Gets the favourite items.
     *
     * @return the favourite items
     * @throws Exception
     *         the exception
     */
    List getFavouriteItems (Item item);

    /**
     * Search.
     *
     * @param itemName
     *         the item name
     * @param item
     *         the item
     * @return the list
     * @throws Exception
     *         the exception
     */
    List search (String itemName, Item item);

    /**
     * Checks if is liked.
     *
     * @param itemId
     *         the item id
     * @param userId
     *         the user id
     * @return the like
     * @throws Exception
     *         the exception
     */
    Like isLiked (int itemId, int userId);

    /**
     * Gets the all activity size.
     *
     * @return the all activity size
     */
    int getAllActivitySize ();

    /**
     * Gets the current index.
     *
     * @return the current index
     */
    int getCurrentIndex ();

    /**
     * Gets the user items.
     *
     * @param userId
     *         the user id
     * @return the user items
     * @throws Exception
     *         the exception
     */
    List <Item> getUserItems (int userId);

    /**
     * Gets the color.
     *
     * @param colorId
     *         the color id
     * @return the color
     * @throws Exception
     *         the exception
     */
    Color getColor (int colorId);

    /**
     * Gets the size.
     *
     * @param sizeId
     *         the size id
     * @return the size
     * @throws Exception
     *         the exception
     */
    Size getSize (int sizeId);

    /**
     * Save notification on like.
     *
     * @param like
     *         the like
     * @return the notification
     * @throws Exception
     *         the exception
     */
    Notification saveNotificationOnLike (Like like);

    /**
     * Save notification on comment.
     *
     * @param comment
     *         the comment
     * @return the notification
     * @throws Exception
     *         the exception
     */
    Notification saveNotificationOnComment (Comment comment);


    /**
     * Unlike.
     *
     * @param userId
     *         the user id
     * @param itemId
     *         the item id
     * @return the int
     */
    void unlike (int userId, int itemId);

    /**
     * Checks if is offered.
     *
     * @param offerBy
     *         the offer by
     * @param itemId
     *         the item id
     * @param offerTo
     *         the offer to
     * @return the offer
     * @throws Exception
     *         the exception
     */
    Offer isOffered (Integer offerBy, Integer itemId, Integer offerTo);

    int getCategoryIdFromItem (int itemId);

    Category getCategory (int categoryId);
}
