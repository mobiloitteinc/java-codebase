package com.mobiloitte.rasras.dao;

import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.Notification;
import com.mobiloitte.rasras.entity.User;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Interface NotificationDao.
 */
public interface NotificationDao {

    /**
     * Gets the notifications.
     *
     * @param notificationManager
     *         the notification manager
     * @return the notifications
     * @throws Exception
     *         the exception
     */
    List <Object[]> getNotifications (Notification notificationManager);

    /**
     * Sets the notification status.
     *
     * @param u1
     *         the u 1
     * @return the int
     * @throws Exception
     *         the exception
     */
    int setNotificationStatus (User u1);

    /**
     * Gets the total results.
     *
     * @return the total results
     */
    int getTotalResults ();

    /**
     * Gets the current index.
     *
     * @return the current index
     */
    int getCurrentIndex ();

    /**
     * Gets the notification.
     *
     * @param notificationId
     *         the notification id
     * @return the notification
     * @throws Exception
     *         the exception
     */
    String getNotification (int notificationId);

    /**
     * Gets the device tokens.
     *
     * @param userId
     *         the user id
     * @return the device tokens
     */
    List <Device> getDeviceTokens (Integer userId);

    /**
     * Gets the.
     *
     * @param itemId
     *         the item id
     * @return the item
     * @throws Exception
     *         the exception
     */
    Item get (Integer itemId);

    void delete (Integer notificationId);

}
