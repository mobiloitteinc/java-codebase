package com.mobiloitte.rasras.dao;

import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.model.ChangePassword;

// TODO: Auto-generated Javadoc

/**
 * The Interface UserDao.
 */
public interface UserDao {

    /**
     * Gets the.
     *
     * @param userId
     *         the user id
     * @return the user
     * @throws Exception
     *         the exception
     */
    User get (Integer userId);

    /**
     * Change.
     *
     * @param changePassword
     *         the change password
     * @return the int
     * @throws Exception
     *         the exception
     */
    int change (ChangePassword changePassword);

    /**
     * Update.
     *
     * @param userDetails
     *         the user details
     * @return the user details
     * @throws Exception
     *         the exception
     */
    UserDetails update (UserDetails userDetails);

    /**
     * Verify.
     *
     * @param otp
     *         the otp
     * @return the string
     * @throws Exception
     *         the exception
     */
    String verify (OTP otp);

    /**
     * Gets the.
     *
     * @param id
     *         the id
     * @return the user details
     * @throws Exception
     *         the exception
     */
    UserDetails get (int id);

    /**
     * Gets the user with item.
     *
     * @param userId
     *         the user id
     * @return the user with item
     * @throws Exception
     *         the exception
     */
    User getUserWithItem (Integer userId);


}
