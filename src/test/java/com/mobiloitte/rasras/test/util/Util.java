package com.mobiloitte.rasras.test.util;

import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Util {

    public static List <User> getUserList () {

        List <User> users = new ArrayList <>();
        User user = new User();
        UserDetails userDetails = new UserDetails();
        Device device = new Device();
        Set <Device> devices = new HashSet <>();

        user.setUserId(1);
        user.setEmail("email1@testing.com");
        user.setMobileNumber("9000000001");
        user.setPassword("testingpassword");

        userDetails.setUserDetailsId(1);
        userDetails.setFirstName("testingfirstname");
        userDetails.setLastName("testinglastname");
        userDetails.setAddress("testingaddress");
        userDetails.setLatitude(28.87);
        userDetails.setLongitude(76.45);

        device.setDeviceId(101);
        device.setDeviceKey("testingDeviceKey");
        device.setDeviceToken("testingDeviceToken");
        device.setDeviceType("testingDeviceType");
        device.setUserId(1);

        devices.add(device);

        user.setUserDetails(userDetails);
        user.setDevices(devices);

        users.add(user);

        return users;
    }

    public static Map <String, Object> getSuccessResponse () {
        Map <String, Object> response = new HashMap <>();
        response.put(Message.CODE, Code.SUCCESS);
        return response;
    }
}
