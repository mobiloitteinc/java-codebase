package com.mobiloitte.rasras.test.controller;


import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.model.ChangePassword;
import com.mobiloitte.rasras.rest.controller.SecureLoginController;
import com.mobiloitte.rasras.service.UserService;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Message;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SecureLoginControllerTest {

    @Mock
    private
    LoginService loginService;

    @Mock
    private
    UserService userService;

    @Mock
    private
    StaticContent staticContent;

    @Mock
    private
    StaticService staticService;

    @Mock
    private
    ChangePassword changePassword;

    @Mock
    private
    HttpServletRequest request;

    @Mock
    private
    User user;

    @InjectMocks
    private
    SecureLoginController secureLoginController;

    @Captor
    private
    ArgumentCaptor <StaticContent> staticContentCaptor;

    @Captor
    private
    ArgumentCaptor <User> userCaptor;

    @Captor
    private
    ArgumentCaptor <ChangePassword> changePasswordCaptor;

    @Captor
    private
    ArgumentCaptor <HttpServletRequest> requestCaptor;

    @Spy
    private
    Map <String, Object> successResponse = new HashMap <>();

    @Before
    public void setUp () {
        MockitoAnnotations.initMocks(this);
        successResponse = getSuccessResponse();
    }

    @Test
    public void signup () throws Exception {
        when(loginService.save(any(User.class), any(HttpServletRequest.class))).thenReturn(successResponse);
        Assert.assertEquals(secureLoginController.signup(user, request).get(Message.CODE), Code.SUCCESS);
        verify(loginService, times(1)).save(userCaptor.capture(), requestCaptor.capture());
    }

    @Test
    public void signin () throws Exception {
        when(loginService.login(any(User.class), any(HttpServletRequest.class))).thenReturn(successResponse);
        Assert.assertEquals(secureLoginController.login(user, request).get(Message.CODE), Code.SUCCESS);
        verify(loginService, times(1)).login(userCaptor.capture(), requestCaptor.capture());
    }

    @Test
    public void forgotPassword () throws Exception {
        when(loginService.forgot(any(User.class), any(HttpServletRequest.class))).thenReturn(successResponse);
        Assert.assertEquals(secureLoginController.forgotPassword(user, request).get(Message.CODE), Code.SUCCESS);
        verify(loginService, times(1)).forgot(userCaptor.capture(), requestCaptor.capture());
    }

    @Test
    public void resetPassword () throws Exception {
        when(userService.resetPassword(any(ChangePassword.class), any(HttpServletRequest.class)))
                .thenReturn(successResponse);
        Assert.assertEquals(secureLoginController.resetPassword(changePassword, request).get(Message.CODE),
                Code.SUCCESS);
        verify(userService, times(1)).resetPassword(changePasswordCaptor.capture(), requestCaptor.capture());
    }

    @Test
    public void resendOTP () throws Exception {
        when(loginService.updateOtp(any(Integer.class), any(HttpServletRequest.class))).thenReturn(successResponse);
        Assert.assertEquals(secureLoginController.resendOTP(user.getUserId(), request).get(Message.CODE), Code.SUCCESS);
        verify(loginService, times(1)).updateOtp(user.getUserId(), request);
    }

    @Test
    public void updateLanguage () throws Exception {
        when(loginService.updateLanguage(any(User.class), any(HttpServletRequest.class))).thenReturn(successResponse);
        Assert.assertEquals(secureLoginController.updateLanguage(user, request).get(Message.CODE), Code.SUCCESS);
        verify(loginService, times(1)).updateLanguage(userCaptor.capture(), requestCaptor.capture());
    }

    @Test
    public void staticContent () throws Exception {
        when(staticService.getContent(any(StaticContent.class), any(HttpServletRequest.class))).thenReturn(successResponse);
        Assert.assertEquals(secureLoginController.getContent(staticContent, request).get(Message.CODE), Code.SUCCESS);
        verify(staticService, times(1)).getContent(staticContentCaptor.capture(), requestCaptor.capture());
    }

    public Map <String, Object> getSuccessResponse () {
        Map <String, Object> response = new HashMap <>();
        response.put(Message.CODE, Code.SUCCESS);
        return response;
    }


}
