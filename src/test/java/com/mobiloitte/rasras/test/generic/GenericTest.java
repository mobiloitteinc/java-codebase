package com.mobiloitte.rasras.test.generic;


import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.test.DateTimeFormatValidation;
import com.mobiloitte.rasras.test.EmailValidator;
import com.mobiloitte.rasras.test.FirstNameValidator;
import com.mobiloitte.rasras.test.FutureValidation;
import com.mobiloitte.rasras.test.LastNameValidator;
import com.mobiloitte.rasras.test.MaxLengthValidatior;
import com.mobiloitte.rasras.test.MinimumLengthValidator;
import com.mobiloitte.rasras.test.MobileValidator;
import com.mobiloitte.rasras.test.NotEmptyValidator;
import com.mobiloitte.rasras.test.NumberValidation;
import com.mobiloitte.rasras.test.PasswordValidator;
import com.mobiloitte.rasras.test.PastValidator;
import com.mobiloitte.rasras.test.PinCodeValidator;
import com.mobiloitte.rasras.test.PostalCodeValidationAustralia;
import com.mobiloitte.rasras.test.SearchValidator;
import com.mobiloitte.rasras.test.SizeValidator;
import com.mobiloitte.rasras.test.Util;
import com.mobiloitte.rasras.test.ZipCodeValidatior;
import com.mobiloitte.rasras.test.ZipcodeValidationUk;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//import com.mobiloitte.rasras.test.URlValidator;


@RunWith(MockitoJUnitRunner.class)
public class GenericTest {

    @Spy
    User student;

    @InjectMocks
    EmailValidator emailValidator;

    @InjectMocks
    PasswordValidator passwordValidator;

    @InjectMocks
    FirstNameValidator firstNameValidator;

    @InjectMocks
    LastNameValidator lastNameValidator;

    @InjectMocks
    MobileValidator mobileValidator;

    @InjectMocks
    ZipCodeValidatior zipCodeValidatior;

    @InjectMocks
    MaxLengthValidatior maxLengthValidatior;

    @InjectMocks
    MinimumLengthValidator minimumLengthValidator;

    @InjectMocks
    NotEmptyValidator notEmptyValidator;

    @InjectMocks
    NumberValidation numberValidation;

    @InjectMocks//8010006651
            SearchValidator searchValidator;


    @InjectMocks
    DateTimeFormatValidation dateTimeFormatValidation;

    @InjectMocks
    PastValidator pastValidator;

    @InjectMocks
    SizeValidator sizeValidator;

    @InjectMocks
    FutureValidation futureValidation;

	/*@InjectMocks
    URlValidator uRLValidator;*/

    @InjectMocks
    PinCodeValidator pinCodeValidator;

    @InjectMocks
    ZipcodeValidationUk zipcodeValidationUk;

    @InjectMocks
    PostalCodeValidationAustralia postalCodeValidationAustralia;


    @Before
    public void setUp () {
        MockitoAnnotations.initMocks(this);
        student = Util.getStudent();
    }

    @Test
    public void emailValidation () {
        Assert.assertEquals(emailValidator.validate(student.getEmail()), true);
    }

    @Test
    public void mobileValidation () {
        Assert.assertEquals(mobileValidator.validatePhoneNumber(student.getMobileNumber()), true);
    }

    @Test
    public void firstNameValidation () {
        Assert.assertEquals(firstNameValidator.textValidation("yatender"), true);
    }

    @Test
    public void lastNameValidation () {
        Assert.assertEquals(lastNameValidator.textValidation("karki"), true);
    }

    @Test
    public void passwordValidation () {
        Assert.assertEquals(passwordValidator.passwordValidation(student.getPassword()), true);
    }


    @Test
    public void maxLengthValidation () {
        Assert.assertEquals(maxLengthValidatior.maxLengthValidation("hello", 40), true);
    }

    @Test
    public void minLengthValidation () {
        Assert.assertEquals(minimumLengthValidator.minLengthValidation("hello", 5), true);
    }

    @Test
    public void notEmptyValidation () {
        Assert.assertEquals(notEmptyValidator.notEmptyValidation("data"), true);
    }

    @Test
    public void numberValidation () {
        Assert.assertEquals(numberValidation.numberValidation("123456789282"), true);
    }


    @Test
    public void dateTimeinDDMMYYYYValidaor () {
        Assert.assertEquals(dateTimeFormatValidation.dateTimeformatValidationinDDMMYYYY("11-22-1425"), true);
    }

    @Test
    public void dateTimeinYYYYMMDDValidaor () {
        Assert.assertEquals(dateTimeFormatValidation.dateTimeformatValidationinYYYYMMDD("1221-22-25"), true);
    }

    @Test
    public void pastDateValidaor () {
        Date dt = new Date();
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        dt = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = sdf.format(dt);
        Assert.assertEquals(pastValidator.pastValidation(strDate), true);
    }

    @Test
    public void sizeValidation () {
        Assert.assertEquals(sizeValidator.sizeValidation(3, 12, "hello"), true);
    }

    @Test
    public void futureValidation () {
        Date dt = new Date();
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 2);
        dt = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = sdf.format(dt);
        Assert.assertEquals(futureValidation.futureValidation(strDate), true);
    }

	/*@Test
    public void urlValidation(){
		Assert.assertEquals(uRLValidator.isValidUrl("http://www.gmail.com"), true);
	}*/

/*
    @Test
	public void pinCodeValidation(){
		Assert.assertEquals(pinCodeValidator.pinCodeValidation("123456"), true);
	}
	
	@Test
	public void zipCodeValidationForUK(){
		Assert.assertEquals(zipcodeValidationUk.zipCodeValidationForUk("li12pp"), true);
	}*/
	

	/*@Test
	public void ZipCodeValidationUAE(){
		Assert.assertEquals(uAEZipCodeValidator.UAEZipCodeValidaton("00000"), true);
	}
	
	@Test
	public void countryCodeValidation(){
		Assert.assertEquals(countryCurrencyCodeValidation.countryCurrencyCodevalidator("Australia", "AUD"), true);
	}*/
}
