package com.mobiloitte.rasras.test.serviceimpl;


import com.mobiloitte.rasras.dao.UserDao;
import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.security.model.JwtUser;
import com.mobiloitte.rasras.test.util.Util;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Email;
import com.mobiloitte.rasras.util.JwtTokenGenerator;
import com.mobiloitte.rasras.util.Message;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceImplTest {

    @Mock
    SMS sms;
    @Mock
    private
    User user;
    @Mock
    private
    LoginDao loginDao;
    @Mock
    private
    UserDao userDao;
    @Mock
    private
    HttpServletRequest request;
    @Mock
    private
    Email email;
    @Mock
    private
    JwtTokenGenerator jwtTokenGenerator;

    @Mock
    private
    MessageSource messageSource;

    @Mock
    private
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    private
    LoginServiceImpl loginServiceImpl;

    @Spy
    private
    List <User> users = new ArrayList <>();

    @Spy
    private
    Map <String, Object> successResponse = new HashMap <>();

    @Captor
    private
    ArgumentCaptor <User> userCaptor;

    @Before
    public void setUp () {
        MockitoAnnotations.initMocks(this);
        users = Util.getUserList();
        successResponse = Util.getSuccessResponse();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void save () throws Exception {
        when(jwtTokenGenerator.generate(any(JwtUser.class))).thenReturn("");
        when(messageSource.getMessage(any(String.class), any(Object[].class), any(Locale.class))).thenReturn("");
        when(email.getEmailTemplate(any(Map.class))).thenReturn(null);
        when(loginDao.save(any(User.class))).thenReturn(1);
        Assert.assertEquals(loginServiceImpl.save(users.get(0), request).get(Message.CODE), Code.SUCCESS);
        verify(loginDao, times(1)).save(userCaptor.capture());
    }
/*
    @Test
    public void login () throws Exception {
        when(bCryptPasswordEncoder.matches(any(String.class), any(String.class))).thenReturn(true);
        when(user.getPassword()).thenReturn("testingPassword");
        when(user.getUserId()).thenReturn(1);
        when(loginDao.login(any(User.class))).thenReturn(user);
        when(user.getStatus()).thenReturn(true);

        Assert.assertEquals(loginServiceImpl.login(users.get(0), request).get(Message.CODE), Code.SUCCESS);
        verify(loginDao, times(1)).login(userCaptor.capture());
    }*/

    @Test
    public void logout () throws Exception {
        when(loginDao.logout(any(Integer.class), any(Device.class))).thenReturn(1);
        Assert.assertEquals(loginServiceImpl.logout(users.get(0), request).get(Message.CODE), Code.SUCCESS);
    }

    @Test
    public void updateOtp () throws Exception {
        when(userDao.get(any(Integer.class))).thenReturn(user);
        when(loginDao.updateOtp(any(Integer.class), any(String.class))).thenReturn(1);
        when(user.getStatus()).thenReturn(true);
        Assert.assertEquals(loginServiceImpl.updateOtp(users.get(0).getUserId(), request).get(Message.CODE), Code.SUCCESS);
    }

    @Test
    public void forgotPassword () throws Exception {
        when(user.getStatus()).thenReturn(true);
        when(user.getMobileNumber()).thenReturn("900000000100");
        when(loginDao.forgot(any(User.class))).thenReturn(user);
        Assert.assertEquals(loginServiceImpl.forgot(users.get(0), request).get(Message.CODE), Code.SUCCESS);
    }

    @Test
    public void updateLanguage () throws Exception {
        when(userDao.get(any(Integer.class))).thenReturn(user);
        when(user.getStatus()).thenReturn(true);
        when(loginDao.updateLanguage(any(Integer.class), any(String.class))).thenReturn(1);
        Assert.assertEquals(loginServiceImpl.updateLanguage(users.get(0), request).get(Message.CODE), Code.SUCCESS);
    }


}
