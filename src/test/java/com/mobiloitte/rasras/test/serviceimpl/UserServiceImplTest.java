package com.mobiloitte.rasras.test.serviceimpl;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.mobiloitte.rasras.dao.ItemDao;
import com.mobiloitte.rasras.dao.UserDao;
import com.mobiloitte.rasras.entity.Item;
import com.mobiloitte.rasras.entity.User;
import com.mobiloitte.rasras.entity.UserDetails;
import com.mobiloitte.rasras.model.ChangePassword;
import com.mobiloitte.rasras.serviceimpl.UserServiceImpl;
import com.mobiloitte.rasras.test.util.Util;
import com.mobiloitte.rasras.util.Code;
import com.mobiloitte.rasras.util.Common;
import com.mobiloitte.rasras.util.Message;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    @Mock
    Item item;
    @Mock
    MessageSource messageSource;
    @Mock
    private
    User user;
    @Mock
    private
    UserDao userDao;
    @Mock
    private
    ItemDao itemDao;
    @Mock
    private
    UserDetails userDetails;
    @InjectMocks
    private
    UserServiceImpl userServiceImpl;
    @Mock
    private
    HttpServletRequest request;
    @Mock
    private
    List <Image> images;
    @Mock
    private
    Common common;
    @Mock
    private
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Mock
    private
    ChangePassword changePassword;
    @Mock
    private
    OTP otp;
    @Spy
    private
    Map <String, Object> successResponse = new HashMap <>();
    @Spy
    private
    List <User> users = new ArrayList <>();
    @Captor
    private
    ArgumentCaptor <Integer> integerCaptor;
    @Captor
    private
    ArgumentCaptor <UserDetails> userDetailsCaptor;

    @Before
    public void setUp () {
        MockitoAnnotations.initMocks(this);
        users = Util.getUserList();
        successResponse = Util.getSuccessResponse();
    }

    @Test
    public void getUser () throws Exception {
        when(userDao.getUserWithItem(any(Integer.class))).thenReturn(user);
        when(itemDao.getItemImages(any(Integer.class))).thenReturn(images);
        when(userDao.get(any(Integer.class))).thenReturn(user);
        when(user.getStatus()).thenReturn(true);
        Assert.assertEquals(userServiceImpl.get(users.get(0).getUserId(), request).get(Message.CODE), Code.SUCCESS);
        verify(userDao, times(1)).getUserWithItem(integerCaptor.capture());
    }

    @Test(expected = JsonMappingException.class)
    public void changePassword () throws Exception {
        when(userDao.get(any(Integer.class))).thenReturn(user);
        when(userDao.change(any(ChangePassword.class))).thenReturn(1);
        when(bCryptPasswordEncoder.matches(any(String.class), any(String.class))).thenReturn(true);
        when(changePassword.getCurrentPassword()).thenReturn("testChangePassword");
        when(user.getPassword()).thenReturn("testChangePassword");
        Assert.assertEquals(userServiceImpl.change(changePassword, request).get(Message.CODE), Code.SUCCESS);
    }

    @Test
    public void update () throws Exception {
        when(common.upload(any(String.class))).thenReturn("testingImage");
        when(userDao.update(any(UserDetails.class))).thenReturn(userDetails);
        when(userDao.get(any(Integer.class))).thenReturn(user);
        when(user.getStatus()).thenReturn(true);
        Assert.assertEquals(userServiceImpl.update(users.get(0).getUserDetails(), request).get(Message.CODE),
                Code.SUCCESS);
        verify(userDao, times(1)).update(userDetailsCaptor.capture());
    }

    @Test(expected = JsonMappingException.class)
    public void verifyOtp () throws Exception {
        when(userDao.verify(any(OTP.class))).thenReturn("");
        Assert.assertEquals(userServiceImpl.verify(otp, request).get(Message.CODE), Code.SUCCESS);

    }

    @Test(expected = JsonMappingException.class)
    public void resetPassword () throws Exception {
        when(userDao.change(any(ChangePassword.class))).thenReturn(1);
        Assert.assertEquals(userServiceImpl.resetPassword(changePassword, request).get(Message.CODE), Code.SUCCESS);
    }

}
